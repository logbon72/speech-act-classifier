/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.features;

import java.util.HashMap;

/**
 *
 * @author intelWorX
 */
//feature position, double, frequency{to multiply by weight later} in document
public class DocumentFeatureVector extends HashMap<Integer, Double>{
    
    private String classLabel;

    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

    public String getClassLabel() {
        return classLabel;
    }
    
}
