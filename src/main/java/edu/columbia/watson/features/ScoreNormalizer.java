/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

/**
 *
 * @author intelWorX
 */
public class ScoreNormalizer {

    private final double low;
    private final double high;
    private final double newHigh;
    private final double newLow;

    public ScoreNormalizer(double low, double high, double newL, double newH) {
        this.low = low;
        this.high = high;
        newHigh = newH;
        newLow = newL;
    }

    public double normalizeScore(double score) {
        if (Double.isInfinite(score) || Double.isNaN(score)) {
            return Double.NEGATIVE_INFINITY;
        }
        return newHigh - ((newHigh - newLow) * (high - score) / (high - low));
    }

}
