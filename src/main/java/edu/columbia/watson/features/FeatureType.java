/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

import java.util.List;
import edu.columbia.watson.doc.Document;

/**
 *
 * @author intelWorX
 */
abstract public class FeatureType {

    public static final int MODE_TRAINING = 0x1;
    public static final int MODE_TESTING = 0x2;
    protected int defaultOrder = 1;

    public String getTypeName() {
        return getClass().getSimpleName();
    }

    public int defaultOrder() {
        return defaultOrder;
    }

    /**
     *
     * @param sentence
     * @param extractMode
     * @param featureList
     */
    abstract public void extractAll(Document sentence, int extractMode, List<Feature> featureList);

    abstract public Feature createFeature(String[] parameters);

    public static boolean isInMode(int currentMode, int checkMode) {
        return (currentMode & checkMode) != 0;
    }

    @Override
    public String toString() {
        return getTypeName(); //To change body of generated methods, choose Tools | Templates.
    }

    
}
