/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.features;

/**
 *
 * @author intelWorX
 */
public class ClassLabel {
    
    protected String label;

    public ClassLabel(String label) {
        this.label = label == null ? null : label.intern();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label.intern();
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof ClassLabel) && ((ClassLabel) obj).label.equals(label); 
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.label != null ? this.label.hashCode() : 0);
        return hash;
    }
    
    
}
