/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

import java.util.ArrayList;
import java.util.List;
import edu.columbia.watson.doc.Document;

/**
 *
 * @author intelWorX
 */
public class FeatureExtractor {

    private int extractMode = 0;

    protected List<FeatureType> featureTypes;

    public FeatureExtractor() {
        this(0);
    }

    public FeatureExtractor(int mode) {
        extractMode = mode;
        //List<FeatureType> featureTypeTmp = FeatureTypeFactory.INSTANCE.getFeatureTypes();
        featureTypes = FeatureTypeFactory.INSTANCE.getFeatureTypes();
        //System.out.println("Feature types: "+featureTypes+" size: "+featureTypes.size());
    }

    public int getExtractMode() {
        return extractMode;
    }

    public void setExtractMode(int extractMode) {
        this.extractMode = extractMode;
    }

    public List<Feature> extractAllFeatures(Document doc) {
        ArrayList<Feature> features = new ArrayList<>();
        for (FeatureType ft : featureTypes) {
            ft.extractAll(doc, extractMode, features);
        }

        return features;
    }

    public List<FeatureType> getFeatureTypes() {
        return featureTypes;
    }

}
