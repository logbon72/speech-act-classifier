/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

/**
 *
 * @author intelWorX
 */
abstract public class Feature {

    protected double featureWeight = 1;
    protected String featureType;

    protected int position = -1;

    abstract public String getHash();

    public double getFeatureWeight() {
        return featureWeight;
    }

    public void setFeatureWeight(double featureWeight) {
        this.featureWeight = featureWeight;
    }

    @Override
    public String toString() {
        return getHash();
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    abstract public String getFeatureType();

    abstract public String[] getParams();

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj ==null){
            return false;
        }
        return (obj instanceof Feature) && ((Feature) obj).getHash().equals(getHash()) ;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.featureWeight) ^ (Double.doubleToLongBits(this.featureWeight) >>> 32));
        hash = 83 * hash + (this.featureType != null ? this.featureType.hashCode() : 0);
        hash = 83 * hash + this.position;
        return hash;
    }

    
}
