/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

import edu.columbia.watson.Configuration;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author intelWorX
 */
public class FeatureTypeFactory {

    public final static FeatureTypeFactory INSTANCE = new FeatureTypeFactory();
    //public static HashMap<String, String> featureTypes = new HashMap<String, String>();
    private final ArrayList<FeatureType> availableFeatureTypes = new ArrayList<>();

    private FeatureTypeFactory() {
        init();
    }

    private void init() {
        String allowedFeatures = Configuration.instance().getProperty("feature.extractor.types", "");
        if (allowedFeatures.length() > 0) {
            String[] classUrls = allowedFeatures.split("[\\s,]+");
            for (String classUrl : classUrls) {
                try {
                    Class<? extends FeatureType> featureType = (Class<? extends FeatureType>) Class.forName(classUrl);
                    availableFeatureTypes.add(featureType.newInstance());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                    Logger.getLogger(FeatureTypeFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public FeatureType getFeatureType(String type) {
        for (FeatureType ft : availableFeatureTypes) {
            if (ft.getTypeName().equalsIgnoreCase(type)) {
                return ft;
            }
        }
        return null;
    }

    public void registerFeatureType(FeatureType ft) {
        availableFeatureTypes.add(ft);
    }

    public List<FeatureType> getFeatureTypes() {
        return availableFeatureTypes;
    }

    public FeatureTypeFactory reload() {
        availableFeatureTypes.clear();
        init();
        return this;
    }

    public boolean isValidFeatureType(String typeName) {
        try {
            return null != getFeatureType(typeName);
        } catch (IllegalStateException ex) {
            return false;
        }
    }

    public Feature createFeature(String typeName, String[] feature) {
        try {
            return getFeatureType(typeName).createFeature(feature);
        } catch (IllegalStateException | NullPointerException ex) {
            return null;
        }
    }

}
