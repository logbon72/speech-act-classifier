/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.features;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import net.sf.javaml.core.SparseInstance;

/**
 *
 * @author intelWorX
 */
public class FeatureList extends HashMap<String, FeatureListEntry> {
    
    final public static FeatureList INSTANCE = new FeatureList();
    
    private FeatureList() {
        super();
    }
    
    public void loadFromTsv(String pathToFile, boolean clear) throws FileNotFoundException {
        loadFromTsv(new FileInputStream(pathToFile), clear);
    }
    
    public void loadFromTsv(File file, boolean clear) throws FileNotFoundException {
        loadFromTsv(new FileInputStream(file), clear);
    }
    
    public void loadFromTsv(InputStream inputStream, boolean clear) {
        Scanner scanner = new Scanner(inputStream);
        if (clear) {
            clear();
        }
        
        while (scanner.hasNextLine()) {
            String[] tmp = scanner.nextLine().trim().split("\t");
            if (tmp.length > 2) {
                put(tmp[2], new FeatureListEntry(Integer.parseInt(tmp[0]), Double.parseDouble(tmp[1])));
            }
        }
    }
    
    public static SparseInstance getSparseRepresentation(List<Feature> features, Object cl) {
        
        SparseInstance sparseInstance = new SparseInstance();
        for (Feature f : features) {
            FeatureListEntry fle = INSTANCE.get(f.getHash());
            if (fle != null) {
                sparseInstance.put(fle.getId(), fle.getWeight());
            }
        }
        sparseInstance.setClassValue(cl);
        return sparseInstance;
    }
    
}
