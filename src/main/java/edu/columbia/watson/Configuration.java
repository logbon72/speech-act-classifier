/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author intelWorX
 */
public class Configuration extends Properties {

    protected static String configPath = "config.properties";
    protected static Configuration instance;

    private Configuration() {
        try {
            load(new FileInputStream(configPath));
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void setConfigPath(String configPath) {
        Configuration.configPath = configPath;
    }

    public static Configuration instance(){
        if(instance == null){
            instance = new Configuration();
        }
        return instance;
    }
}
