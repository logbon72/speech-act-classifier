/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.tagger;

import edu.columbia.watson.summarizer.Ngram;
import edu.columbia.watson.summarizer.PosFilter;
import edu.columbia.watson.summarizer.TweetTagger;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class TweetTaggerRun {

    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);
        String str = "";
        TweetTagger tagger = TweetTagger.INSTANCE;
        tagger.setDecoder(TweetTagger.Decoder.GREEDY);
        //DebugPrint.println(PosFilter.NOUN_PHRASE, PosFilter.VERB_PATTERN);
        while(!str.trim().equals("0")){
            System.out.print("enter string to test, 0 to exit ");
            str = inputReader.nextLine();
            //System.out.println("Tokens: "+Twokenize.tokenizeRawTweetText(str));
            System.out.println("Tags: "+tagger.tag(str));
            if(PosFilter.qualifies(new Ngram(StringUtils.split(str)), "com")){
                System.out.println("Qualifies");
            }else{
                System.out.println("No");
            }
        }
    }
}
