/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.impl.doc.SpeechActDataSet;
import java.io.IOException;

/**
 *
 * @author intelWorX
 */
public class TweetMotifSource extends SpeechActDataSet {

    int currentLine = 0;

    public TweetMotifSource(String path) throws IOException {
        super(path);
    }

    @Override
    public TweetLine getNext() {
        if (fileReader.hasNextLine()) {
            String nextLine = fileReader.nextLine().trim();
            return new TweetLine((++currentLine) + "", null, nextLine);
        }
        return null;
    }
}
