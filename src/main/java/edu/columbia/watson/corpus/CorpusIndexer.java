/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.Configuration;
import edu.columbia.watson.Indexer;
import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.summarizer.Ngram;
import edu.columbia.watson.summarizer.NgramExtractor;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import misc.DebugPrint;
import org.apache.commons.lang.StringUtils;
import org.sqlite.SQLiteConfig;

/**
 *
 * @author intelWorX
 */
public class CorpusIndexer extends HashMap<String, CorpusIndexEntry> implements Indexer<String> {

    public static final int MAX_NGRAM_LENGTH = 1;
    private int totalCount = 0;
    protected static final String corpusPath = Configuration.instance().getProperty("prior.corpus.path", "data/prior.corpus.db");

    protected final static HashMap<String, Integer> wordIdMap = new HashMap<>();

    protected Connection db;
    private boolean isCleared = false;
    public static final int MAX_INSERTS = 256;
    private final String CREATE_COMMAND = "CREATE TABLE [word] (\n"
            + "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
            + "[word] VARCHAR(64)  UNIQUE NULL\n"
            + ");\n"
            + "CREATE TABLE [word_doc] (\n"
            + "[word_id] INTEGER  NOT NULL,\n"
            + "[doc_id] INTEGER  NOT NULL\n"
            + ");\n"
            + "CREATE UNIQUE INDEX [IDX_WORD_DOC_] ON [word_doc](\n"
            + "[word_id]  DESC,\n"
            + "[doc_id]  DESC\n"
            + ");\n"
            + "CREATE INDEX [IDX_WORD_DOC_ID] ON [word_doc](\n"
            + "[doc_id]  DESC\n"
            + ")";

    //private SQLiteConnection connection;
    protected Connection getDb(boolean indexing, boolean clear) {
        return getDb(indexing, clear, false);
    }

    protected Connection getDb(boolean indexing, boolean clear, boolean readonly) {
        try {
            if (db == null || db.isClosed()) {
                try {
                    if (readonly) {
                        SQLiteConfig config = new SQLiteConfig();
                        config.setReadOnly(true);
                        //config.setSynchronous(Syn);
                    }

                    //preload.
                    Class.forName("org.sqlite.JDBC");
                    try {
                        db = DriverManager.getConnection("jdbc:sqlite:" + new File(corpusPath).getAbsolutePath());
                        if (indexing) {
                            //test if table exists
                            //try selecting from table
                            try (Statement statement = db.createStatement()) {
                                statement.execute("pragma encoding=\"UTF-8\"");
                                try {
                                    ResultSet resultSet = statement.executeQuery("SELECT * FROM word LIMIT 1");
                                    if (resultSet.next() && resultSet.getRow() > 0) {
                                        DebugPrint.println("Clearing previous data");
                                        statement.executeUpdate("DELETE FROM word_doc;");
                                        statement.executeUpdate("DELETE FROM word;");
                                    }

                                } catch (SQLException sqe) {
                                    DebugPrint.println("It appears table does not exist", sqe.getMessage());
                                    statement.executeUpdate(CREATE_COMMAND);
                                }
                                statement.close();
                            }
                        }

                    } catch (SQLException ex) {
                        DebugPrint.println("SQLiteException occured:" + ex.getLocalizedMessage());
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorpusIndexer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            return db;
        } catch (SQLException ex) {
            Logger.getLogger(CorpusIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void index(TweetLine tweet) {
        if (db == null) {
            db = getDb(true, !isCleared);
            isCleared = true;
        }

        List<String> tokens = tokenizeTweet(tweet);

        for (int n = 1; n <= Math.min(MAX_NGRAM_LENGTH, tokens.size()); n++) {
            List<Ngram> ngrams = NgramExtractor.extract(tokens, n);
            //saveEntry(tweet.getId(), ngrams, db);
            for (Ngram ngram : ngrams) {
                findEntry(ngram).add(tweet.getId());
            }
            totalCount += ngrams.size();
        }

    }

    protected int getWordId(String word) {
        return getWordId(word, true, null);
    }

    protected int getWordId(String word, boolean create, Connection connection) {
        Integer wordId = wordIdMap.get(word);
        if (wordId == null) {
            if (connection == null) {
                connection = getDb(false, false);
            }
            try {
                try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT id FROM word WHERE word=?", Statement.RETURN_GENERATED_KEYS)) {
                    preparedStatement.setString(1, word);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    if (resultSet.next() && resultSet.getRow() > 0) {
                        wordId = resultSet.getInt(1);
                        wordIdMap.put(word, wordId);
                    } else if (create) {
                        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO word (word) VALUES (?)")) {
                            statement.setString(1, word);
                            ResultSet ids = statement.getGeneratedKeys();
                            if (statement.executeUpdate() > 0 && ids.next()) {
                                wordId = ids.getInt(1);
                                wordIdMap.put(word, wordId);
                            }
                        }
                    }
                }
            } catch (SQLException ex) {
                //ex.printStackTrace();
                //System.exit(-1);
                DebugPrint.println("WordID: SQLQuery error occured: ", ex.getErrorCode(), ex.getMessage());
                return -1;
            }
        }

        return wordId == null ? -1 : wordId;
    }

    public void persist() {
        db = getDb(true, true);
        for (String word : keySet()) {
            saveEntry(word, get(word), db);
        }
    }

    private void saveEntry(String word, CorpusIndexEntry entry, Connection connection) {
        if (!entry.isEmpty()) {
            int wordId = getWordId(word, true, connection);
            try {
                int originalSize = entry.size();
                for (int batch = 1; batch <= Math.ceil(originalSize / MAX_INSERTS); batch++) {
                    int endAt = Math.min(entry.size(), MAX_INSERTS);
                    String insertString = new StringBuilder("REPLACE INTO word_doc VALUES (?, ?)")
                            .append(StringUtils.repeat(", (?, ?)", endAt - 1))
                            .toString();

                    try (PreparedStatement st = connection.prepareStatement(insertString)) {
                        for (int i = 1; i <= endAt; i++) {
                            int j = i * 2;
                            st.setInt(j - 1, wordId);
                            st.setInt(j, Integer.parseInt(entry.pollFirst()));
                        }
                        st.executeUpdate();
                    }
                }
            } catch (SQLException ex) {
                DebugPrint.println("Could not save tweet", ex.getMessage(), " Size: ", entry.size());
            }

        }
    }

    /**
     * Find an Ngrams entry
     *
     * @param ngram
     * @return
     */
    public CorpusIndexEntry findEntry(String ngram) {
        CorpusIndexEntry indexEntry = get(ngram);
        if (indexEntry == null) {
            indexEntry = new CorpusIndexEntry();
            put(ngram, indexEntry);
        }
        return indexEntry;
    }

    public CorpusIndexEntry findEntry(Ngram ngram) {
        return findEntry(ngram.getKey());
    }

    public List<String> tokenizeTweet(TweetLine tweetLine) {
        return tweetLine.getDoc();
    }

    @Override
    public int count(String t) {
        return findEntry(t).size();
    }

    @Override
    public int totalCountOfPhrasesOfLength(int l) {
        if (l == 1) {
            return totalCount;
        }
        throw new UnsupportedOperationException("This class is only for building indexes");
    }

    @Override
    public int totalNumberOfPhrasesOfLength(int l) {
        if (l == 1) {
            return size();
        }
        throw new UnsupportedOperationException("This class is only for building indexes");
    }

    @Override
    public int countCoOccurence(String... words) {
        CorpusIndexEntry retained = new CorpusIndexEntry();
        boolean isFirst = true;
        for (String word : words) {
            if (!NgramExtractor.shouldExclude(word, 1, 0)) {
                if (isFirst) {
                    retained = new CorpusIndexEntry();
                    retained.addAll(findEntry(word.toLowerCase()));
                    isFirst = false;
                } else if (retained.isEmpty()) {
                    return 0;
                } else {
                    retained.retainAll(findEntry(word.toLowerCase()));
                }
            }
        }

        return retained.size();
    }

    @Override
    public int countOccurences(String... words) {
        CorpusIndexEntry union = new CorpusIndexEntry();
        for (String word : words) {
            if (!NgramExtractor.shouldExclude(word, 1, 0)) {
                union.addAll(findEntry(word.toLowerCase()));
            }
        }
        return union.size();
    }
}
