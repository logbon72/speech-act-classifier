/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.doc.TweetLine;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class PriorCorpusFromFile extends CorpusIndexer {

    private static PriorCorpusFromFile instance;

    private int totalCount = 0;
    private int totalDocuments = 0;
    private int avgWordsPerDoc = 0;
    private double avgOccurencePerWord = 0;
    //private int avgWordsPerDoc = 0;

    private PriorCorpusFromFile() {
        super();

        GZIPInputStream scannerIs = null;
        try {
            //int current = 0;
            DebugPrint.println("Loading Corpus from", corpusPath);
            scannerIs = new GZIPInputStream(new FileInputStream(corpusPath));
            Scanner scanner = new Scanner(scannerIs);
            HashSet<String> docList = new HashSet<>();
            while (scanner.hasNextLine()) {
                String[] parts = scanner.nextLine().split("\t");
                if (parts.length > 1) {
                    //parts[1]
                    String[] ids = parts[1].split(",");
                    CorpusIndexEntry entry = buildSet(ids);
                    put(parts[0], entry);
                    totalCount += ids.length;
                    docList.addAll(entry);
                }
            }
            totalDocuments = docList.size();
            avgWordsPerDoc = totalCount / totalDocuments;
            avgOccurencePerWord = totalCount * 1.0 / size();
            docList.clear();
            DebugPrint.println("Done Loading Corpus");
        } catch (IOException ex) {
            Logger.getLogger(PriorCorpusFromFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (scannerIs != null) {
                    scannerIs.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(PriorCorpusFromFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static CorpusIndexEntry buildSet(String[] set) {
        CorpusIndexEntry strings = new CorpusIndexEntry();
        strings.addAll(Arrays.asList(set));
        return strings;
    }

    @Override
    public int totalCountOfPhrasesOfLength(int l) {
        return (avgWordsPerDoc - l + 1) * totalDocuments;
    }

    @Override
    public int totalNumberOfPhrasesOfLength(int l) {
        return (int) Math.round(totalCountOfPhrasesOfLength(l) / avgOccurencePerWord);
    }

    @Override
    public void index(TweetLine tweet) {
        throw new UnsupportedOperationException("Is not used for building tweets");
    }

    public static PriorCorpusFromFile instance() {
        if (instance == null) {
            instance = new PriorCorpusFromFile();
        }
        return instance;
    }

    public int getTotalCount() {
        return totalCount;
    }

}
