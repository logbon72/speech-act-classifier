/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.doc.TweetSource;
import edu.columbia.watson.summarizer.TweetSummarizer;
import java.io.*;
import java.util.Scanner;

/**
 *
 * @author intelWorX
 */
public class S140TweetSource implements TweetSource {

    public static final int PARTS = 6;
    private Scanner inputFileReader;
    protected int skipLines;
    protected int numLines;
    protected int currentLine = 0;
    private final String filePath;

    public S140TweetSource(String inputFilePath, int skipLines) throws FileNotFoundException, IOException {
        filePath = inputFilePath;
        this.skipLines = skipLines;
        this.numLines = countNumLines(filePath);
        initialize();
    }

    final protected void initialize() throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(filePath);
        currentLine = 0;
        this.inputFileReader = new Scanner(fis);
        while (currentLine < skipLines) {
            inputFileReader.nextLine();
            currentLine++;
        }
    }

    final protected int countNumLines(String fPath) throws FileNotFoundException, IOException {
        File infile = new File(fPath);
        //System.out.println("Absolute Path: "+infile.getAbsolutePath());
        LineNumberReader lnr = new LineNumberReader(new FileReader(infile));
        while (lnr.skip(Long.MAX_VALUE) > 0) {
        }
        return lnr.getLineNumber();
    }

    @Override
    public int count() {
        //just in case skipLinex > numLines
        return Math.max(numLines - skipLines, 0);
    }

    @Override
    public boolean hasNext() {
        return inputFileReader.hasNextLine();
    }

    protected String[] nextLine(int limit) {
        currentLine++;
        String[] nextLine = inputFileReader.nextLine().trim().split("\\s*,", limit);
        String[] op = new String[nextLine.length];
        for (int i = 0; i < nextLine.length; i++) {
            op[i] = nextLine[i].replaceAll("(^\")|(\"$)", "");
        }
        return op;
    }

    public int getCurrentLine() {
        return currentLine;
    }

    @Override
    public void reset() {
        try {
            inputFileReader.close();
            initialize();
        } catch (FileNotFoundException ex) {
        }
    }

    @Override
    public TweetLine getNext() {
        String lines[] = nextLine(PARTS);
        if (lines.length >= PARTS) {
            TweetLine tweet = new TweetLine(lines[1], lines[0], lines[5]);
            //TweetSummarizer.preprocessTweet(tweet);
            return tweet;
        }
        return null;
    }
}
