/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.doc.TweetSource;
import edu.columbia.watson.impl.doc.SpeechActTrainnigSet;
import edu.columbia.watson.summarizer.TweetSummarizer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import misc.DebugPrint;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class CorpusBuilder {

    public static void main(String[] args) throws IOException {
        //long start = System.currentTimeMillis();
        //DebugPrint.println("Starting at ", start);
        //PriorCorpus priorCorpus = PriorCorpus.instance();
        //DebugPrint.println("Completed in ", System.currentTimeMillis() - start);
        //System.exit(0);

        List<TweetSource> tweetSources = new ArrayList<>();
        tweetSources.add(new TweetMotifSource("../tweetmotif/everything_else/data/all_background"));
        tweetSources.add(new SpeechActTrainnigSet());
        tweetSources.add(new S140TweetSource("../s140data/corpus.csv", 0));

        CorpusIndexer indexer = new CorpusIndexer();
        int currentLine = 0;
        for (TweetSource tweetSource : tweetSources) {
            DebugPrint.println("Indexing with source: ", tweetSource.getClass().getSimpleName());
            DebugPrint.println("total number of lines: ", tweetSource.count());
            DebugPrint.println("indexing...");
            while (tweetSource.hasNext()) {
                if ((currentLine++) % 10000 == 0) {
                    DebugPrint.println("Currently indexing,", currentLine);
                    System.gc();
                }

                TweetLine tweet = tweetSource.getNext();
                tweet.setId(currentLine + "");
                TweetSummarizer.preprocessTweet(tweet);
                indexer.index(tweet);
                tweet.getDoc().clear();
            }
            DebugPrint.println("Indexed: ", currentLine);
        }
        DebugPrint.println("Persisting");
        //indexer.persist();
        DebugPrint.println("Done");
        String corpusFile = "data/prior.corpus.csv";
        DebugPrint.println("Writing corpus file, ngram count: ", indexer.size());
        try (Formatter corpusWriter = new Formatter(corpusFile)) {
            int currentNgram = 0;
            int ngramCount = indexer.size();

            for (String ngram : indexer.keySet()) {
                if ((currentNgram++) % 10000 == 0) {
                    DebugPrint.println("Writing NGRAM", currentNgram, " of ", ngramCount);
                    if (currentNgram > 1) {
                        corpusWriter.flush();
                    }
                }
                for (String tweetId : indexer.get(ngram)) {
                    corpusWriter.format("%s,%s%n", ngram, tweetId);
                }
            }

            corpusWriter.flush();
        }
    }
}
