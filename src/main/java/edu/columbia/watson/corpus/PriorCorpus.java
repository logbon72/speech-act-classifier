/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.corpus;

import edu.columbia.watson.doc.TweetLine;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class PriorCorpus extends CorpusIndexer {

    private static PriorCorpus instance;

    private int totalCount = 0;
    //hard coded for performance reason and since the number is unlike to change
    //as the corpus is only.
    private final int totalDocuments = 560000;
    private int avgWordsPerDoc = 0;
    private double avgOccurencePerWord = 0;
    private int uniqueWords;
    //private int avgWordsPerDoc = 0;

    private PriorCorpus() {
        super();
        try {
            long start = System.currentTimeMillis();
            DebugPrint.println("Prior loading...");
            db = getDb(false, false, true);
            //db.setReadOnly(true);
            DebugPrint.println("Initialized...");
            Statement statement = db.createStatement();
            try (ResultSet rs = statement.executeQuery("select count(*) from word_doc")) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            

//            try (ResultSet rs = statement.executeQuery("select count(DISTINCT doc_id) from word_doc")) {
//                if (rs.next()) {
//                    totalDocuments = rs.getInt(1);
//                }
//            }
            try (ResultSet rs = statement.executeQuery("select count(*) from word")) {
                if (rs.next()) {
                    uniqueWords = rs.getInt(1);
                }
            }

            avgWordsPerDoc = totalCount / totalDocuments;
            avgOccurencePerWord = totalCount / uniqueWords;

            DebugPrint.println("Prior loaded in ", System.currentTimeMillis() - start, "ms");
            //register close down
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        if(db != null && !db.isClosed()){
                            db.close();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(PriorCorpus.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            });

        } catch (SQLException ex) {
            DebugPrint.println("SQLite Exception: ", ex.getMessage());
        }

    }

    @Override
    public int totalCountOfPhrasesOfLength(int l) {
        return (avgWordsPerDoc - l + 1) * totalDocuments;
    }

    @Override
    public int totalNumberOfPhrasesOfLength(int l) {
        return (int) Math.round(totalCountOfPhrasesOfLength(l) / avgOccurencePerWord);
    }

    @Override
    public void index(TweetLine tweet) {
        throw new UnsupportedOperationException("Is not used for building tweets");
    }

    public static PriorCorpus instance() {
        if (instance == null) {
            instance = new PriorCorpus();
        }
        return instance;
    }

    public int getTotalCount() {
        return totalCount;
    }

    @Override
    public CorpusIndexEntry findEntry(String ngram) {
        CorpusIndexEntry entry = get(ngram);
        if (entry == null) {
            entry = new CorpusIndexEntry();
            int wordId = getWordId(ngram, false, db);
            if (wordId > 0) {
                loadEntries(wordId, entry);
            }
            put(ngram, entry);
        }

        return entry;
    }

    private void loadEntries(int wordId, CorpusIndexEntry entry) {
        try (PreparedStatement statement = db.prepareStatement("SELECT doc_id FROM word_doc WHERE word_id=?")) {
            statement.setInt(1, wordId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                entry.add(resultSet.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PriorCorpus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int size() {
        return uniqueWords;
    }

}
