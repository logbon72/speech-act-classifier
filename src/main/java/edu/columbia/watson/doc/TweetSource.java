/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.doc;

/**
 *
 * @author intelWorX
 */
public interface TweetSource {

    public abstract int count();

    public abstract boolean hasNext();

    public abstract TweetLine getNext();

    public abstract void reset();

}
