/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.doc;

import cmu.arktweetnlp.Twokenize;
import edu.columbia.watson.preprocessor.PreprocessorList;
import edu.columbia.watson.preprocessor.list.*;
import java.util.HashSet;
import java.util.Objects;

/**
 *
 * @author intelWorX
 */
public class TweetLine {

    private String label;
    private String text;
    private String id;
    private Document doc;
    private static final HashSet<String> preprocessors = new HashSet<>(5);

    static {
        preprocessors.add(LowerCaseTweet.class.getSimpleName());
        preprocessors.add(ReplaceURL.class.getSimpleName());
        preprocessors.add(RemoveUsername.class.getSimpleName());
        preprocessors.add(ReplaceRepeatedLetters.class.getSimpleName());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TweetLine(String id, String label, String text) {
        this.label = label;
        this.text = text;
        this.id = id;
    }

    public TweetLine(String label, String text) {
        this(null, label, text);
    }

    public String getLabel() {
        return label;
    }

    public String getText() {
        return text;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * Twitter document loaded lazily.
     *
     * @return
     */
    public Document getDoc() {
        if (doc == null) {
            //doc = whatever
            doc = new Document();
            String processed = PreprocessorList.preProcess(text, "", PreprocessorList.PROCESS_TRAINING, preprocessors);
            doc.addAll(Twokenize.tokenizeRawTweetText(processed));
            doc.setOriginal(text);
        }
        return doc;

    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TweetLine){
            TweetLine ref = (TweetLine) obj;
            if(ref.id != null && ref.id.equals(id)){
                return true;
            }
            return ref.text != null && ref.text.equals(text);
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.text);
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        return id + ": "+ text; //To change body of generated methods, choose Tools | Templates.
    }
    
}
