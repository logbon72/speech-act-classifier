/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.doc;

import java.util.ArrayList;

/**
 *
 * @author intelWorX
 */
public class Document extends ArrayList<String> {
    private String original;

    public void setOriginal(String original) {
        this.original = original;
    }
    
    public String getOriginal(){
        return original;
    }
}
