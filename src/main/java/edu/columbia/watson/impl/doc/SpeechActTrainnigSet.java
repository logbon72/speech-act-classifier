/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.impl.doc;

import edu.columbia.watson.Configuration;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author intelWorX
 */
public class SpeechActTrainnigSet extends SpeechActDataSet {

    public SpeechActTrainnigSet() throws IOException {
        this(Configuration.instance().getProperty("speechact.training.file", "data/speechact_train.txt"));
    }
    
    public SpeechActTrainnigSet(String path) throws IOException{
        super(path);
    }
    
    
}
