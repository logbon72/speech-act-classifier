/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.doc;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.doc.TweetSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;

/**
 *
 * @author intelWorX
 */
abstract public class SpeechActDataSet implements TweetSource {

    protected Scanner fileReader;
    protected final String filePath;

    protected int lineCount;

    public static final String DELIM = "\\s+::\\s?";

    public SpeechActDataSet(String pathToFile) throws FileNotFoundException, IOException {
        filePath = pathToFile;
        initSource();
    }

    private void initSource() throws FileNotFoundException, IOException {
        fileReader = new Scanner(new FileInputStream(new File(filePath)));
        lineCount = countNumLines(filePath);
    }

    public void setFileReader(Scanner fileReader) {
        this.fileReader = fileReader;
    }

    public Scanner getFileReader() {
        return fileReader;
    }

    final protected int countNumLines(String fPath) throws FileNotFoundException, IOException {
        File infile = new File(fPath);
        //System.out.println("Absolute Path: "+infile.getAbsolutePath());
        LineNumberReader lnr = new LineNumberReader(new FileReader(infile));
        while (lnr.skip(Long.MAX_VALUE) > 0) {
        }
        return lnr.getLineNumber();
    }

    @Override
    public int count() {
        return lineCount;
    }

    @Override
    public TweetLine getNext() {
        if (fileReader.hasNextLine()) {
            String nextLine = fileReader.nextLine().trim();
            String parts[] = nextLine.split(DELIM);
            if (parts.length > 1) {
                return new TweetLine(parts[parts.length - 1], parts[0]);
            }
        }
        return null;
    }

    @Override
    public boolean hasNext() {
        return fileReader.hasNextLine();
    }

    @Override
    public void reset() {
        try {
            initSource();
        } catch (IOException e) {
        }
    }
}
