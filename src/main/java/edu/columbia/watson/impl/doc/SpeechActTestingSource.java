/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.impl.doc;

import edu.columbia.watson.Configuration;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author intelWorX
 */
public class SpeechActTestingSource extends SpeechActDataSet{

    public SpeechActTestingSource(String pathToFile) throws FileNotFoundException, IOException {
        super(pathToFile);
    }

    public SpeechActTestingSource() throws FileNotFoundException, IOException {
        super(Configuration.instance().getProperty("speechact.testing.file", "data/speechact_test.txt"));
    }
    
    
    
}
