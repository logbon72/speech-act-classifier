/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.doc.Document;
import edu.columbia.watson.features.Feature;
import edu.columbia.watson.features.FeatureType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author intelWorX
 */
public class SymbolAtStartFeatureType extends FeatureType {

    public static final ArrayList<String> SYMBOLS = new ArrayList<>();

    static {
        SYMBOLS.add("rt ");
        SYMBOLS.add("#");
        SYMBOLS.add("@");
    }

    @Override
    public void extractAll(Document sentence, int extractMode, List<Feature> featureList) {
        String original = sentence.getOriginal();
        if (original != null && original.length() > 0) {
            original = original.toLowerCase();
            for (String symbol : SYMBOLS) {
                if (original.startsWith(symbol)) {
                    featureList.add(new SymbolAtStartFeature(symbol.trim()));
                    return;
                }
            }
        }
    }

    @Override
    public Feature createFeature(String[] parameters) {
        return new SymbolAtStartFeature(parameters[0]);
    }

}
