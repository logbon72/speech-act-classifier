/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.doc.Document;
import edu.columbia.watson.features.Feature;
import edu.columbia.watson.features.FeatureType;
import java.util.List;

/**
 *
 * @author intelWorX
 */
public class TermFeatureType extends FeatureType {

//    String stopwordString = "a,about,after,again,all,am,an,and,any,are,as,at,be,because,been,both,by,could,did,do,does,doing,for,from,had,has,have,he,her,here,hers,herself,him,himself,his,how,i,if,in,into,is,it,its,itself,me,my,myself,no,nor,not,of,off,on,once,only,or,other,ought,our,ours ,ourselves,out,over,own,same,she,should,so,some,such,than,that,the,their,theirs,them,themselves,then,there,there's,these,they,they'd,they'll,they're,they've,this,those,through,to,too,under,until,up,very,was,we,we'd,we'll,we're,we've,were,what,what's,when,when's,where,where's,which,while,who,who's,whom,why,why's,with,would,you,you'd,you'll,you're,you've,your,yours,yourself,yourselves";
    /**
     *
     */
    //public static ArrayList<String> stopwords = new ArrayList<>();
    public TermFeatureType() {
//        if (stopwords.isEmpty()) {
//            
//        }

    }

//    private static void loadStopWords(){
//        String pathToStopwords = Configuration.instance().getProperty("stopwords.file.path");
//        if(pathToStopwords != null){
//            
//        }
//        
//    }
    @Override
    public void extractAll(Document doc, int extractMode, List<Feature> features) {
        //works with all modes
        int pos = 0;
        for (String word : doc) {
            //String stemmedVersion = Stemmer.stemToken(word);
            Feature termFeature = new TermFeature(word);
            termFeature.setPosition(pos);
//                if (isInMode(extractMode, MODE_TESTING)) {
//                    if (featureRegistry.hasFeature(termFeature)) {
//                        termFeature.setFeatureWeight(featureRegistry.findFeatureEntry(termFeature).getFeature().getFeatureWeight());
//                    }
//                }
            features.add(termFeature);
            ++pos;
        }
    }

    @Override
    public Feature createFeature(String[] parameters) {
        return parameters.length > 0 ? new TermFeature(parameters[0]) : null;
    }

    public TermFeature findFeatureAt(int pos, List<Feature> features) {
        for (Feature f : features) {
            if (f instanceof TermFeature && f.getPosition() == pos) {
                return (TermFeature) f;
            }
        }
        return null;
    }

}
