/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.doc.Document;
import edu.columbia.watson.features.Feature;
import edu.columbia.watson.features.FeatureType;
import java.util.List;
import misc.Stemmer;

/**
 *
 * @author intelWorX
 */
public abstract class NGramType extends FeatureType {

    public static final String PADDING = "*";
    public static final String STOP = "STOP";
    public static final String NGRAM_DELIMETER = "||";

    protected int n = 0;

    public abstract void setN();

    @Override
    public void extractAll(Document sentence, int extractMode, List<Feature> features) {
        setN();
        int length = sentence.size();
        if (n > 0) {
            for (int i = 0; i <= (length - n); i++) {//8
                String ngram[] = new String[n];
                for (int j = 0; j < n; j++) {//0-1
                    int k = i + j;
                    ngram[j] = sentence.get(k);
                }

                Ngram feature = new Ngram(ngram);
                feature.setPosition(i);
//                if ((extractMode & MODE_TESTING) != 0) {
//                    if (featureRegistry.hasFeature(feature)) {
//                        feature.setFeatureWeight(featureRegistry.findFeatureEntry(feature).getFeature().getFeatureWeight());
//                    }
//                }

                features.add(feature);
            }
        }
    }

    @Override
    public Feature createFeature(String[] parameters) {
        return new Ngram(parameters);
    }

}
