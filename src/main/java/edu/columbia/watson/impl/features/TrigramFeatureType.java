/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

/**
 *
 * @author intelWorX
 */
public class TrigramFeatureType extends NGramType {

    @Override
    public void setN() {
        n = 3;
    }

}
