/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.features.Feature;
import misc.StringUtils;

/**
 *
 * @author intelWorX
 */
public class Ngram extends Feature {

    protected String[] words;
    protected String hash;

    public Ngram(String[] ngram) {
        words = new String[ngram.length];
        for (int i = 0; i < ngram.length; i++) {
            words[i] = ngram[i].intern();
        }
        hash = (words.length + "_G|" + StringUtils.join(NGramType.NGRAM_DELIMETER, ngram)).intern();
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public String[] getParams() {
        return words;
    }

    @Override
    public String getFeatureType() {
        return words.length == 2 ? BigramFeatureType.class.getSimpleName() : TrigramFeatureType.class.getSimpleName();
    }

}
