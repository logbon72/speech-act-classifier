/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.features.Feature;

/**
 *
 * @author intelWorX
 */
public class SymbolOccurence extends Feature {

    final private static String FEATURE_TYPE = SymbolOccurenceType.class.getSimpleName();

    public static final String OCCUR_NONE = "0";
    public static final String OCCUR_ONCE_TWICE = "1";
    public static final String OCCUR_MORE_THAN_TWICE = "2";

    private String occurence;
    private String symbol;
    private String hash;

    public SymbolOccurence(String symbol, int count) {
        String occur;
        switch (count) {
            case 0:
                occur = OCCUR_NONE;
                break;
            case 1:
            case 2:
                occur = OCCUR_ONCE_TWICE;
                break;
            default:
                occur = OCCUR_MORE_THAN_TWICE;
        }
        init(symbol, occur);
    }

    public SymbolOccurence(String symbol, String occurence) {
        init(symbol, occurence);
    }

    private void init(String symbol, String occurence) {
        this.occurence = occurence;
        this.symbol = symbol;
        hash = new StringBuilder("hasSym:")
                .append(symbol)
                .append(":")
                .append(occurence)
                .toString().intern();
    }

    public String getOccurence() {
        return occurence;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public String getFeatureType() {
        return FEATURE_TYPE;
    }

    @Override
    public String[] getParams() {
        return new String[]{symbol, occurence};
    }

}
