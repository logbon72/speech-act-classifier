/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.doc.Document;
import edu.columbia.watson.features.Feature;
import edu.columbia.watson.features.FeatureType;
import edu.columbia.watson.posttokenize.Emorjis;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import misc.DebugPrint;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class SymbolOccurenceType extends FeatureType {

    public static final ArrayList<String> SYMBOLS = new ArrayList<>();
    public static final ArrayList<String> SYMBOLS_MATCH_EXACT = new ArrayList<>();

    public static final String EMORJI = "EMOT";
    public static final String NUM = "NUM";
    public static final String URL = "URL";
    public static final Pattern NUM_REGEX = Pattern.compile("^\\d+(\\.\\d+)*$");
    public static final Pattern URL_REGEX = Pattern.compile("^https?://");

    static {
        //SYMBOLS.add("rt");
        SYMBOLS.add("#");
        SYMBOLS.add("@");
        SYMBOLS.add("!");
        SYMBOLS.add("?");
        SYMBOLS_MATCH_EXACT.add("rt");
    }

    @Override

    public void extractAll(Document sentence, int extractMode, List<Feature> featureList) {
        if (!sentence.isEmpty()) {
            HashMap<String, Integer> counts = getBlankCounts();
            for (String word : sentence) {
                for (String symbol : SYMBOLS) {
                    if (word.contains(symbol)) {
                        counts.put(symbol, counts.get(symbol) + StringUtils.countMatches(word, symbol));
                    }
                }

                for (String symbol : SYMBOLS_MATCH_EXACT) {
                    if (word.equalsIgnoreCase(symbol)) {
                        counts.put(symbol, counts.get(symbol) + 1);
                    }
                }

                if (Emorjis.isEmorji(word)) {
                    counts.put(EMORJI, counts.get(EMORJI) + 1);
                } else if (URL_REGEX.matcher(word).find()) {
                    counts.put(URL, counts.get(URL) + 1);
                } else if (NUM_REGEX.matcher(word).find()) {
                    counts.put(NUM, counts.get(NUM) + 1);
                }
            }

            for (String symbol : counts.keySet()) {
                featureList.add(new SymbolOccurence(symbol, counts.get(symbol)));
            }
        }
    }

    private HashMap<String, Integer> getBlankCounts() {
        HashMap<String, Integer> counts = new HashMap<>(SYMBOLS.size());
        for (String s : SYMBOLS) {
            counts.put(s.intern(), 0);
        }

        for (String s : SYMBOLS_MATCH_EXACT) {
            counts.put(s.intern(), 0);
        }

        counts.put(EMORJI, 0);
        counts.put(NUM, 0);
        counts.put(URL, 0);
        return counts;
    }

    @Override
    public Feature createFeature(String[] parameters) {
        return new SymbolOccurence(parameters[0], parameters[1]);
    }

}
