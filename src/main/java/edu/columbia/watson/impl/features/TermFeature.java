/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.features.Feature;

/**
 *
 * @author intelWorX
 */
public class TermFeature extends Feature {

    protected String baseTerm;
    protected String hash;
    public static String TYPE_NAME = null;

    @Override
    public String getFeatureType() {
        return TYPE_NAME;
    }

    public TermFeature(String baseTerm) {
        if (TYPE_NAME == null) {
            TermFeatureType tf = new TermFeatureType();
            TYPE_NAME = tf.getTypeName();
        }
        setBaseTerm(baseTerm);
    }

    @Override
    public String getHash() {
        return hash;
    }

    public String getBaseTerm() {
        return baseTerm;
    }

    final public void setBaseTerm(String baseTerm) {
        this.baseTerm = baseTerm.intern();
        hash = ("1GRAM:" + baseTerm).intern();
    }

    @Override
    public String[] getParams() {
        return new String[]{baseTerm};
    }

}
