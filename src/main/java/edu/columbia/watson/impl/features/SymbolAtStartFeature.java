/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.impl.features;

import edu.columbia.watson.features.Feature;

/**
 *
 * @author intelWorX
 */
public class SymbolAtStartFeature extends Feature {

    private static final String FEATURE_TYPE = SymbolAtStartFeatureType.class.getSimpleName();

    private final String symbol;
    private final String hash;

    public SymbolAtStartFeature(String symbol) {
        this.symbol = symbol;
        hash = new StringBuilder("ATSTART:")
                .append(symbol)
                .toString().intern();
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public String getFeatureType() {
        return FEATURE_TYPE;
    }

    @Override
    public String[] getParams() {
        return new String[]{symbol};
    }

}
