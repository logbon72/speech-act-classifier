/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.posttokenize;

import edu.columbia.watson.Configuration;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import misc.Log;

/**
 *
 * @author intelWorX
 */
public class NoiseWords {

    protected static final HashSet<String> noiseWords = new HashSet<>();

    static {
        String filePath = Configuration.instance().getProperty("noisewords.file.path", "data/noisewords.txt");
        try {
            Scanner reader = new Scanner(new File(filePath));
            String tmp;
            while (reader.hasNextLine()) {
                tmp = reader.nextLine().trim();
                if (tmp.length() > 0) {
                    noiseWords.add(tmp.toLowerCase());
                }
            }
        } catch (FileNotFoundException ex) {
            Log.getLogger().log(Level.SEVERE, null, ex);
        }
    }
    
    public static boolean isNoise(String word){
        return noiseWords.contains(word.toLowerCase());
    }
    
}
