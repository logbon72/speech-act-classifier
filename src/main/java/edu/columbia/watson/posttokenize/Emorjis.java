/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.posttokenize;

import edu.columbia.watson.Configuration;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author intelWorX
 */
public class Emorjis {

    protected final static HashSet<String> emorjis = new HashSet();

    static {
        String emorjisFile = Configuration.instance().getProperty("emorjis.file.path", "data/emoticons.txt");
        try {
            List<String> read = FileUtils.readLines(new File(emorjisFile), Charsets.UTF_8);
            emorjis.addAll(read);
        } catch (IOException ex) {
            Logger.getLogger(Emorjis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean isEmorji(String str) {
        return emorjis.contains(str.toLowerCase());
    }

}
