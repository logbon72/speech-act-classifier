/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.posttokenize;

import edu.columbia.watson.Configuration;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author intelWorX
 */
public class SlangsList {

    protected final static HashMap<String, String> slangsMap = new HashMap<>();

    static {
        //initialize slangs file
        //load slangs
        String slangsPath = Configuration.instance().getProperty("slangs.file.path", "data/slangs.txt");
        try {
            Scanner slangsReader = new Scanner(new File(slangsPath));
            while (slangsReader.hasNextLine()) {
                String[] tmp = slangsReader.nextLine().trim().split("\t");
                if (tmp.length > 1) {
                    slangsMap.put(tmp[0].toLowerCase(), tmp[1]);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SlangsList.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static int replaceSlangs(List<String> tokens) {

        String token, repl;
        int replaced = 0;
        for (int i = 0; i < tokens.size(); i++) {
            token = tokens.get(i).toLowerCase();
            repl = slangsMap.get(token);
            if (repl != null) {
                tokens.set(i, repl);
                replaced++;
            }
        }
        return replaced;
    }

}
