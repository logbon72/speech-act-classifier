/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import java.util.HashMap;
import java.util.Objects;

/**
 *
 * @author intelWorX
 */
public class NgramGraphNode implements Comparable<NgramGraphNode> {

    final private Ngram ngram;
    final HashMap<NgramGraphNode, Integer> adj = new HashMap<>();

    public NgramGraphNode(Ngram ngram) {
        this.ngram = ngram;
    }

    public Ngram getNgram() {
        return ngram;
    }

    public HashMap<NgramGraphNode, Integer> getAdj() {
        return adj;
    }

    public boolean hasEdgeTo(NgramGraphNode targetNode) {
        return edgeWeight(targetNode) > 0;
    }

    public int edgeWeight(NgramGraphNode targetNode) {
        Integer weight = adj.get(targetNode);
        if (weight != null) {
            return weight;
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NgramGraphNode) {
            return ((NgramGraphNode) obj).ngram.equals(ngram);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.ngram);
        return hash;
    }

    @Override
    public String toString() {
        return new StringBuilder("{<")
                .append(ngram)
                .append(">, ")
                .append(adj)
                .append("}")
                .toString();
    }

    @Override
    public int compareTo(NgramGraphNode o) {
        if (o.ngram.getSalience() < ngram.getSalience()) {
            return -1;
        } else if (o.ngram.getSalience() > ngram.getSalience()) {
            return 1;
        } else {
            return 0;
        }
    }

}
