/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import java.util.Objects;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class Ngram {

    final private String words[];
    final private String key;
    final public int length;
    private double salience = 0.0;
    private double likelihoodScore = 0.0;
    private double graphScore = 0.0;

    public Ngram(String[] words) {
        this.words = words;
        key = StringUtils.join(words, " ").toLowerCase().intern();
        length = words.length;
    }

    public String getKey() {
        return key;
    }

    public String[] getWords() {
        return words;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Ngram) && ((Ngram) obj).key.equals(key);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.key);
        return hash;
    }

    public int getLength() {
        return length;
    }

    public double getSalience() {
        return salience;
    }

    public void setSalience(double salience) {
        this.salience = salience;
    }

    public void setLikelihoodScore(double likelihoodScore) {
        this.likelihoodScore = likelihoodScore;
    }

    public double getGraphScore() {
        return graphScore;
    }

    public void setGraphScore(double graphScore) {
        this.graphScore = graphScore;
    }

    public double getLikelihoodScore() {
        return likelihoodScore;
    }

    @Override
    public String toString() {
        return StringUtils.join(words, " ");
    }

    
}
