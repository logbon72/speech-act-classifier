/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.summarizer;

import java.util.ArrayList;

/**
 *
 * @author intelWorX
 */
public class NgramGraph extends ArrayList<NgramGraphNode>{
    
    public NgramGraphNode addNode(Ngram ngram){
        NgramGraphNode node = new NgramGraphNode(ngram);
        add(node);
        return node;
    }
}
