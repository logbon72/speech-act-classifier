/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.speechact.SpeechActClassifier;
import edu.columbia.watson.summarizer.score.JaccardSimilarity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class MoreGenericTemplateFiller extends TemplateFiller {

    static {
        VERB_FRAMES.put(SpeechActClassifier.ACT_COM, " comment on ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_QUE, " are asking about ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_STA, " mention ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_SUG, " suggest ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_MISC, " are talking about ");
    }

    protected static final int NGRAMS_PER_ACT = 3;
    HashSet<String> actsFilled = new HashSet<>();

    public MoreGenericTemplateFiller(TweetIndexer indexer, NgramGraph topNgrams, String topic) {
        super(indexer, topNgrams, topic);
    }

    @Override
    public String buildSummary(List<TweetLine> representativeTweets) {
        selectedNgrams.clear();
        selectTemplateFillers();
        StringBuilder output = new StringBuilder("For ")
                .append(topic)
                .append(", people");

        if (!selectedNgrams.isEmpty()) {
            tweetsByAct = groupSelectedByAct();
            int actCount = tweetsByAct.size();
            int current = 0;
            for (String act : orderedActs) {

                if (++current != 1) {
                    //output.append(", ");
                    if (current == actCount) {
                        output.append(". And ");
                    } else {
                        output.append(". They");
                    }

                }

                output.append(VERB_FRAMES.get(act));
                List<Ngram> phrases = tweetsByAct.get(act);
                ArrayList<String> strings = new ArrayList<>(tweetsByAct.get(act).size());
                //generate components for this act
                int d = 0;
                int phrasesSize = phrases.size();
                for (Ngram phrase : phrases) {
                    String norm = phrase.getKey().contains("#")
                            ? splitter.split(phrase.getKey())//has hash tag? use key, splitter reduces to words anyways.
                            : StringUtils.join(phrase.getWords(), " ");
                    strings.add((++d == phrasesSize && phrasesSize > 1 ? "and {" : "{") + norm + "}");
                }

                output.append(StringUtils.join(strings, ", "));
            }
        }

        fillRepresentative(representativeTweets);
        return renderedTemplate = output.toString();
    }

    @Override
    protected void selectTemplateFillers() {
        int totalLength = 0;
        while (totalLength < SUMMARY_MAX_LENGTH && actsFilled.size() != tweetsByAct.size() && !allEmpty(tweetsByAct.values())) {
            for (String act : tweetsByAct.keySet()) {
                List<Ngram> actNgrams = tweetsByAct.get(act);
                if (!actNgrams.isEmpty() && !isActFilled(act)) {
                    Ngram topNgram = null;
                    for (int i = 0; i < actNgrams.size(); i++) {
                        if (actNgrams.get(i).length != 1 || hasOnlyUnigrams(act, actNgrams)) {
                            topNgram = actNgrams.remove(i);
                            break;
                        }
                    }

                    if (topNgram != null) {
                        int phraseLength = topNgram.getKey().length();
                        if (!isRedundant(topNgram)) {
                            //select
                            selectedNgrams.put(topNgram, act);
                        } else {
                            phraseLength = 0;
                        }

                        totalLength += phraseLength;
                    }
                }
            }
        }
    }

    protected boolean isActFilled(String act) {
        if (!actsFilled.contains(act)) {
            int selected = Collections.frequency(selectedNgrams.values(), act);
            if (selected >= NGRAMS_PER_ACT) {
                actsFilled.add(act);
                return true;
            } else {
                return false;
            }
        }

        return true;
    }
}
