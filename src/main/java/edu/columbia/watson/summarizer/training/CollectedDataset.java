/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer.training;

import edu.columbia.watson.corpus.S140TweetSource;
import edu.columbia.watson.doc.TweetLine;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author intelWorX
 */
public class CollectedDataset extends S140TweetSource {

    public static final int FIELDS = 5;

    public CollectedDataset(String pathToFile) throws FileNotFoundException, IOException {
        super(pathToFile, 0);
    }

    @Override
    public TweetLine getNext() {
        String lines[] = nextLine(FIELDS);
        if (lines.length >= FIELDS) {
            TweetLine tweet = new TweetLine(lines[0], null, lines[1]);
            return tweet;
        }
        return null;
    }

}
