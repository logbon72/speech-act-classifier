/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.python.HashtagSplitter;
import java.util.Scanner;

/**
 *
 * @author intelWorX
 */
public class CheckHashtagSplitter {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.print("Enter Hastag to split: ");
            String hashtag = scanner.nextLine();
            if(hashtag.trim().length() < 1){
                break;
            }
            
            System.out.println("Result: "+ HashtagSplitter.getInstance().split(hashtag));
        } while (true);
    }
}
