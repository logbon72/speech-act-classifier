/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.speechact.SpeechActClassifier;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import misc.DebugPrint;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class PosFilter {

    final public static String NOUN_PATTERN = "(XDN|DAN|AN|DN|N)+".replace("D", "[D\\$]").replace("N", "[NO\\ˆSZ]");
    final public static String NOUN_PHRASE = "^" + NOUN_PATTERN + "$";
    final public static String VERB_PATTERN = "^(NVPN|LVN|NV+NT|NV+TN|NV+R|NV+N|VN|RV|V+)$".replace("N", NOUN_PATTERN);
    final public static Pattern NP_REGEX = Pattern.compile(NOUN_PHRASE);
    final public static Pattern VCP_REGEX = Pattern.compile(VERB_PATTERN);

    public static List<Ngram> filter(List<Ngram> ngrams, TweetIndexer indexer) {
        Iterator<Ngram> ngIt = ngrams.iterator();
        while (ngIt.hasNext()) {
            Ngram ngram = ngIt.next();
            String act = indexer.getAct(ngram);
            if (!qualifies(ngram, act)) {
                ngIt.remove();
            }
        }

        return ngrams;
    }

    public static boolean qualifies(Ngram ngram, final String act) {
        List<String> tags = TweetTagger.INSTANCE.tag(ngram.toString());
        String tagPatern = StringUtils.join(tags, "");
        switch (act) {
            case SpeechActClassifier.ACT_MISC:
            case SpeechActClassifier.ACT_STA:
            case SpeechActClassifier.ACT_COM:
                //@todo check opinion words
                return NP_REGEX.matcher(tagPatern).find();

            case SpeechActClassifier.ACT_SUG:
                boolean result = VCP_REGEX.matcher(tagPatern).find();
                //DebugPrint.println(act, "Tag ", tagPatern, " result", result);
                return result;
            case SpeechActClassifier.ACT_QUE:
                return NP_REGEX.matcher(tagPatern).find() || VCP_REGEX.matcher(tagPatern).find();
            default:
                return false;
        }
    }
}
