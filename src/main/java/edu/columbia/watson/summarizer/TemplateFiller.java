/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.python.HashtagSplitter;
import edu.columbia.watson.speechact.SpeechActClassifier;
import edu.columbia.watson.summarizer.score.JaccardSimilarity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class TemplateFiller {

    protected final static HashMap<String, String> VERB_FRAMES = new HashMap<>();
    protected final static double JACCARD_THRESHOLD = 0.35;
    protected final static double THRESHOLD_TWEET_OVERLAP = 0.6;
    protected final static int SUMMARY_MAX_LENGTH = 250;
    protected final static int REP_TWEETS_PER_PHRASE = 2;

    static {
        VERB_FRAMES.put(SpeechActClassifier.ACT_COM, " comment on ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_QUE, " are asking about ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_STA, " mention ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_SUG, " suggest ");
        VERB_FRAMES.put(SpeechActClassifier.ACT_MISC, " say stuffs like ");
    }

    final protected TweetIndexer indexer;
    protected HashMap<String, List<Ngram>> tweetsByAct;
    final protected String topic;
    protected HashMap<Ngram, String> selectedNgrams = new HashMap<>();
    protected HashSet<String> actsWithOnlyOneLeft = new HashSet<>();
    protected String renderedTemplate;
    static HashtagSplitter splitter = HashtagSplitter.getInstance();
    ArrayList<String> orderedActs = new ArrayList<>();

    public TemplateFiller(TweetIndexer indexer, NgramGraph topNgrams, String topic) {
        this.indexer = indexer;
        this.topic = topic.contains("#") ? splitter.split(topic) : topic;
        tweetsByAct = groupByNodesAct(topNgrams);
    }

    public String buildSummary(List<TweetLine> representativeTweets) {
        selectedNgrams.clear();
        selectTemplateFillers();
        if (selectedNgrams.isEmpty()) {
            return renderedTemplate = "Not enough data to generate good summary.";
        }

        StringBuilder output = new StringBuilder("For ")
                .append(topic)
                .append(", people");

        if (!selectedNgrams.isEmpty()) {
            tweetsByAct = groupSelectedByAct();
            int actCount = tweetsByAct.size();
            int current = 0;
            for (String act : orderedActs) {

                if (++current != 1) {
                    output.append(", ");
                    if (current == actCount) {
                        output.append("and ");
                    }
                    output.append("they");
                }

                output.append(VERB_FRAMES.get(act));
                List<Ngram> phrases = tweetsByAct.get(act);
                if (phrases != null) {
                    ArrayList<String> strings = new ArrayList<>(phrases.size());
                    //generate components for this act
                    for (Ngram phrase : phrases) {
                        String norm = phrase.getKey().contains("#")
                                ? splitter.split(phrase.getKey())//has hash tag? use key, splitter reduces to words anyways.
                                : StringUtils.join(phrase.getWords(), " ");
                        strings.add("{" + norm + "}");
                    }
                    
                    output.append(StringUtils.join(strings, ", "));
                }
            }
        }

        fillRepresentative(representativeTweets);
        return renderedTemplate = output.toString();
    }

    protected void fillRepresentative(List<TweetLine> representativeTweets) {
        if (representativeTweets != null) {
            List<Ngram> sortedSet = new ArrayList<>(selectedNgrams.keySet());

            Collections.sort(sortedSet, new Comparator<Ngram>() {
                @Override
                public int compare(Ngram o1, Ngram o2) {
                    return ((Double) o2.getSalience()).compareTo(o1.getSalience());
                }
            });

            for (Ngram ngram : sortedSet) {
                int added = 0;
                for (TweetLine tweet : indexer.findEntry(ngram)) {
                    if (!representativeTweets.contains(tweet) && !isRedundant(tweet, representativeTweets)) {
                        representativeTweets.add(tweet);
                        ++added;
                    }
                    if (added > REP_TWEETS_PER_PHRASE) {
                        break;
                    }
                }
            }
        }
    }

    protected void selectTemplateFillers() {
        int totalLength = 0;

        while (totalLength < SUMMARY_MAX_LENGTH && !allEmpty(tweetsByAct.values())) {
            for (String act : tweetsByAct.keySet()) {
                List<Ngram> actNgrams = tweetsByAct.get(act);
                if (!actNgrams.isEmpty()) {
                    Ngram topNgram = null;
                    for (int i = 0; i < actNgrams.size(); i++) {
                        if (actNgrams.get(i).length != 1 || hasOnlyUnigrams(act, actNgrams)) {
                            topNgram = actNgrams.remove(i);
                            break;
                        }
                    }

                    if (topNgram != null) {
                        int phraseLength = topNgram.getKey().length();
                        if (!isRedundant(topNgram)) {
                            //select
                            selectedNgrams.put(topNgram, act);
                        } else {
                            phraseLength = 0;
                        }

                        totalLength += phraseLength;
                    }
                }
            }
        }
    }

    protected boolean hasOnlyUnigrams(String act, List<Ngram> ngrams) {

        if (actsWithOnlyOneLeft.contains(act)) {
            return true;
        }

        for (Ngram ngram : ngrams) {
            if (ngram.length > 1) {
                return false;
            }
        }

        actsWithOnlyOneLeft.add(act);
        return true;
    }

    /**
     * checks if a phrase is redundant by computing jaccard's threshold.
     *
     * @param ngram
     * @return
     */
    protected boolean isRedundant(Ngram ngram) {
        if (!selectedNgrams.isEmpty()) {
            for (Ngram selectedNgram : selectedNgrams.keySet()) {
                int intersection = JaccardSimilarity.countIntersection(selectedNgram, ngram);
                if (intersection > 0 && JaccardSimilarity.score(ngram, selectedNgram) >= JACCARD_THRESHOLD) {
                    return true;
                }
            }
        }

        return false;
    }

    protected boolean isRedundant(TweetLine tweet, List<TweetLine> selectedTweets) {
        String tweetText = tweet.getText().toLowerCase();
        for (TweetLine aTweet : selectedTweets) {
            String aTweetText = aTweet.getText().toLowerCase();
            int intersection = JaccardSimilarity.countIntersection(aTweetText, tweetText);
            if (intersection > 0 && JaccardSimilarity.score(aTweetText, tweetText) >= THRESHOLD_TWEET_OVERLAP) {
                return true;
            }
        }

        return false;
    }

    protected boolean allEmpty(Collection<? extends Collection> collections) {
        if (!collections.isEmpty()) {
            for (Collection c : collections) {
                if (c.isEmpty()) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public HashMap<Ngram, String> getSelectedNgrams() {
        return selectedNgrams;
    }

    private HashMap<String, List<Ngram>> groupByNodesAct(NgramGraph salientNgramsGraph) {
        HashMap<String, List<Ngram>> grouping = new HashMap<>();
        for (NgramGraphNode node : salientNgramsGraph) {
            String act = indexer.getAct(node.getNgram());
            List<Ngram> ngramsInAct = grouping.get(act);
            if (ngramsInAct == null) {//first time encountering act
                ngramsInAct = new ArrayList<>();
                grouping.put(act, ngramsInAct);
                orderedActs.add(act);//we need the order to be maintained
            }
            ngramsInAct.add(node.getNgram());
        }
        return grouping;
    }

    protected HashMap<String, List<Ngram>> groupSelectedByAct() {
        HashMap<String, List<Ngram>> grouping = new HashMap<>();
        for (Ngram ngram : selectedNgrams.keySet()) {
            String act = indexer.getAct(ngram);
            List<Ngram> ngramsInAct = grouping.get(act);
            if (ngramsInAct == null) {
                ngramsInAct = new ArrayList<>();
                grouping.put(act, ngramsInAct);
            }
            ngramsInAct.add(ngram);
        }
        return grouping;
    }

}
