/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import cmu.arktweetnlp.Tagger;
import cmu.arktweetnlp.Twokenize;
import cmu.arktweetnlp.impl.ModelSentence;
import cmu.arktweetnlp.impl.Sentence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import misc.Log;

/**
 *
 * @author intelWorX
 */
public class TweetTagger {

    public static final String modelFile = "/cmu/arktweetnlp/model.20120919";
    public static TweetTagger INSTANCE = new TweetTagger();

    protected Tagger tagger;

    protected boolean loaded;

    public static enum Decoder {

        GREEDY, VITERBI
    };

    private Decoder decoder = Decoder.GREEDY;

    private TweetTagger() {
        try {
            tagger = new Tagger();
            tagger.loadModel(modelFile);
            loaded = true;
        } catch (IOException ex) {
            Log.getLogger().severe("Could not find model file.");
        }
    }

    public List<String> tag(String text) {
        Sentence sentence = new Sentence();

        sentence.tokens = Twokenize.tokenizeRawTweetText(text);
        ModelSentence modelSentence = null;

        if (sentence.T() > 0) {
            modelSentence = new ModelSentence(sentence.T());
            tagger.featureExtractor.computeFeatures(sentence, modelSentence);
            if (decoder == Decoder.VITERBI) {
                tagger.model.viterbiDecode(modelSentence);
            } else {
                tagger.model.greedyDecode(modelSentence, false);
            }
        }

        ArrayList<String> labels = new ArrayList<>();
        if (modelSentence != null) {
            for (int t = 0; t < sentence.T(); t++) {
                labels.add(tagger.model.labelVocab.name(modelSentence.labels[t]));
            }
        }

        return labels;
    }

    public void setDecoder(Decoder decoder) {
        this.decoder = decoder;
    }

}
