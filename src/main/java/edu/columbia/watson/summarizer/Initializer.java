/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.summarizer;
import edu.columbia.watson.Configuration;
import edu.columbia.watson.corpus.PriorCorpus;
import edu.columbia.watson.posttokenize.Emorjis;
import misc.*;
/**
 *
 * @author intelWorX
 */
public class Initializer {
    //load corpus
    //load slangs
    //load models
    //instantiate tagger
    //instantiate noise words.
    //load morjis
    //load speech act classifier
    //python scripts hash tag splitter, enity getter
    
    public static void init(String configPath){
        DebugPrint.println("Initializing...");
        DebugPrint.println("Setting config path to: ", configPath);
        Configuration.setConfigPath(configPath);
        DebugPrint.println("Instance Set");
        Configuration.instance();
        DebugPrint.println("Loading prior corpus");
        //PriorCorpus.instance();
        DebugPrint.println("Loading tagger model");
    }
}
