/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import cmu.arktweetnlp.Twokenize;
import edu.columbia.watson.Configuration;
import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.doc.TweetSource;
import edu.columbia.watson.posttokenize.Emorjis;
import edu.columbia.watson.posttokenize.SlangsList;
import edu.columbia.watson.preprocessor.Preprocessor;
import edu.columbia.watson.preprocessor.list.ReplaceRepeatedLetters;
import edu.columbia.watson.preprocessor.list.ReplaceURL;
import edu.columbia.watson.speechact.SpeechActClassifier;
import edu.columbia.watson.summarizer.score.NgramLikelihoodScore;
import edu.columbia.watson.summarizer.score.SalienceScore;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import misc.DebugPrint;
import net.sf.javaml.classification.Classifier;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class TweetSummarizer {

    protected TweetSource tweetSource;
    protected List<String> urls;
    protected String topic;
    protected TweetIndexer indexer;
    protected static final HashSet<String> FILTERED_CLASSES = new HashSet<>();
    protected static final Preprocessor REPEATED_REMOVER = new ReplaceRepeatedLetters();

    final protected static SpeechActClassifier SPEECH_ACT_CL = SpeechActClassifier.INSTANCE;
    final protected static TweetTagger TAGGER = TweetTagger.INSTANCE;
    final protected static int TOP_TWEETS_PER_LENGTH = 50;
    final protected static int THRESHOLD_FOR_SALIENCE = 10;

    protected List<TweetLine> representativeTweets = new ArrayList<>();

    static {
        Classifier classifier = SpeechActClassifier.DEFAULT__CLASSIFIER;
        Configuration config = Configuration.instance();
        String classifierClass = config.getProperty("speechact.classifier.class", null);
        if (classifierClass != null) {
            try {
                classifier = (Classifier) Class.forName(classifierClass).newInstance();
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(TweetSummarizer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            SPEECH_ACT_CL.init(config.getProperty("training.model.file", "data/model.data"), config.getProperty("training.features.list", "data/featurelist.tsv"), classifier);
        } catch (IOException ex) {
            Logger.getLogger(TweetSummarizer.class.getName()).log(Level.SEVERE, null, ex);
        }

        FILTERED_CLASSES.add(SpeechActClassifier.ACT_MISC);

    }

    protected String generateSummary;

    public static final int DEFAULT_LIMIT = 3000;
    private int tweetLimit;

    public TweetSummarizer(String topic, TweetSource tweetSource, List<String> urls, int tweetLimit) {
        this.tweetSource = tweetSource;
        this.topic = topic;
        this.urls = urls;
        this.tweetLimit = tweetLimit;
    }

    public TweetSummarizer(String topic, TweetSource tweetSource, List<String> urls) {
        this(topic, tweetSource, urls, DEFAULT_LIMIT);
    }

    /**
     * Filter out tweets that do not have majority ACT.
     *
     * @return
     */
    protected List<TweetLine> filterTweetsByAct() {
        DebugPrint.println("Number of tweets in source = ", tweetSource.count());

        List<TweetLine> classifedTweets = SPEECH_ACT_CL.classifyTweets(tweetSource, tweetLimit);
        List<TweetLine> filteredTweets = new ArrayList<>();

        for (TweetLine tweetLine : classifedTweets) {
            if (!FILTERED_CLASSES.contains(tweetLine.getLabel())) {
                filteredTweets.add(tweetLine);
            }
        }

        DebugPrint.println("Number of tweets after filtering = ", filteredTweets.size());

        return filteredTweets;
    }

    protected void classifyNgramTweets(List<Ngram> ngrams) {
        DebugPrint.println("Classifiying tweets in ", ngrams.size(), " top ngrams");

        for (Ngram ngram : ngrams) {
            NgramIndexEntry ne = indexer.findEntry(ngram);
            for (TweetLine tweet : ne) {
                if (tweet.getLabel() == null) {
                    tweet = SPEECH_ACT_CL.classifyTweet(tweet);
                }
                ne.addAct(tweet.getLabel());
                if (ne.getMaxAct() != null) {
                    break;
                }
            }
        }

    }

    protected List<TweetLine> getTweets() {
        List<TweetLine> filteredTweets = new ArrayList<>();

        while (tweetSource.hasNext() && filteredTweets.size() < tweetLimit) {
            filteredTweets.add(tweetSource.getNext());
        }

        DebugPrint.println("Number of tweets retrieved = ", filteredTweets.size());

        return filteredTweets;
    }

    //do preprocessing of tweets...
    public static void preprocessTweets(List<TweetLine> tweets) {
        //replace slangs with equivalent words
        for (TweetLine tweet : tweets) {
            preprocessTweet(tweet);
        }
        //
    }

    public static void preprocessTweet(TweetLine tweet) {

        String newText = REPEATED_REMOVER.process(tweet.getText(), null);

        tweet.setText(newText);
        List<String> tokens = Twokenize.tokenizeRawTweetText(tweet.getText());
        //replace hash tag
        int replaced = 0;
        //replaced = HashtagSplitter.replaceHashTags(tokens);
        replaced += SlangsList.replaceSlangs(tokens);

        if (replaced > 0) {
            //DebugPrint.println("in tweet: ", tweet, " replaced ", replaced);
            newText = StringUtils.join(tokens, " ");
            tweet.setText(newText);
            tokens = Twokenize.tokenize(newText);
        }

        //remove retweet symbol also and emoticons
        //remove URL too
        Iterator<String> tokenIterator = tokens.iterator();
        while (tokenIterator.hasNext()) {
            String nextToken = tokenIterator.next();
            if (nextToken.equalsIgnoreCase("RT")
                    || Emorjis.isEmorji(nextToken)
                    || nextToken.matches(ReplaceURL.p.pattern())) {
                tokenIterator.remove();
            }
        }

        tweet.getDoc().clear();
        tweet.getDoc().addAll(tokens);
    }

    private void buildSummary() {
        //step 1. filter out those not in the class
        List<TweetLine> tweets = getTweets();

        //step 2. perform preprocessing
        DebugPrint.println("Preprocessing tweets");
        preprocessTweets(tweets);

        //step 3.  index tweets
        DebugPrint.println("Indexing tweets");
        indexer = new TweetIndexer(tweets, topic).build();

        List<Ngram> allNgrams = new ArrayList<>(indexer.size());
        allNgrams.addAll(indexer.allNgrams());
        //step 4.1 score by likely hood.
        DebugPrint.println("Scoring NGrams", "Size: ", allNgrams.size());
        NgramLikelihoodScore scorer = new NgramLikelihoodScore(allNgrams, indexer);
        allNgrams = scorer.score().getNgrams();
        //step 4.2 get top ngrams
        List<Ngram> topNgrams = extractTopNgrams(allNgrams);
        allNgrams.clear();
        DebugPrint.println("Top NGrams", "Size: ", topNgrams.size());

        DebugPrint.println("Classifying tweets by ACT for POS filtering");
        //classify tweets for top ngrams
        classifyNgramTweets(topNgrams);

        List<Ngram> topNgramsBackup = new ArrayList<>(topNgrams);
        DebugPrint.println("Performing POS filtering");
        //step 4.3 do pos filtering
        topNgrams = PosFilter.filter(topNgrams, indexer);
        DebugPrint.println("Size after filtering", topNgrams.size());
        
        if(topNgrams.isEmpty() || topNgrams.size() < THRESHOLD_FOR_SALIENCE){
            DebugPrint.println("POS Filtering removed (almost) every thing, restoring back up");
            topNgrams.addAll(topNgramsBackup);
        }
        
        //step 5 build graph score and salience score
        //step 5.1 build graph
        NgramGraph ngramGraph = indexer.buildGraph(topNgrams);
        //step 5.2 do graph score and salience score
        graphScoreIteration(ngramGraph);
        DebugPrint.println("Completed graph score iteration");

        TemplateFiller templateFiller = new MoreGenericTemplateFiller(indexer, ngramGraph, topic);
        generateSummary = templateFiller.buildSummary(representativeTweets);
    }

    private void graphScoreIteration(NgramGraph graph) {
        SalienceScore.computeSalienceScores(graph);
    }

    private List<Ngram> extractTopNgrams(List<Ngram> ngrams) {
        HashMap<Integer, List<Ngram>> ngramsPerPhLength = new HashMap<>(TweetIndexer.MAX_NGRAM_LENGTH);
        for (int i = 1; i <= TweetIndexer.MAX_NGRAM_LENGTH; i++) {
            ngramsPerPhLength.put(i, new ArrayList<Ngram>());
        }

        int totalAdded = 0;
        int totalExpected = TOP_TWEETS_PER_LENGTH * TweetIndexer.MAX_NGRAM_LENGTH;
        for (Ngram ngram : ngrams) {
            List<Ngram> bin = ngramsPerPhLength.get(ngram.length);
            if (ngram.getLikelihoodScore() > 0 && bin.size() < TOP_TWEETS_PER_LENGTH) {
                ++totalAdded;
                bin.add(ngram);
            }
            if (totalAdded >= totalExpected) {
                break;
            }
        }

        ArrayList<Ngram> topNgrams = new ArrayList<>();
        for (List<Ngram> bin : ngramsPerPhLength.values()) {
            topNgrams.addAll(bin);
        }

        return topNgrams;
    }

    /**
     * Get the summary, if it does not exist, generate it.
     *
     * @return
     */
    public String getSummary() {
        if (generateSummary == null) {
            buildSummary();
        }
        return generateSummary;
    }

    public List<TweetLine> getRepresentativeTweets() {
        return representativeTweets;
    }

}
