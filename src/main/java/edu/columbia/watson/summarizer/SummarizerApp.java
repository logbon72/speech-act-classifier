/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.corpus.PriorCorpus;
import edu.columbia.watson.summarizer.training.CollectedDataset;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import misc.DebugPrint;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author intelWorX
 */
public class SummarizerApp {

    public static String tweetExtractorPath = "../tweetextractor/out";

    public static void main(String[] args) {
        if (args.length > 0) {
            tweetExtractorPath = args[0];
        }

//        System.out.println(PriorCorpus.instance().findEntry("astros"));
//        System.exit(0);
        Scanner inputReader = new Scanner(System.in);

        String str;
        TweetSummarizer tweetSummarizer;
        while (true) {
            try {
                System.out.print("Enter topic, (0 to exit) ");
                str = inputReader.nextLine().trim();
                if (str.equals("0")) {
                    break;
                }
                String topicBaseFile = str.replaceAll("[^A-Za-z0-9_-]+", "_").toLowerCase();
                String topic = str;
                DebugPrint.println("Topic file: ", topicBaseFile);
                List<String> lines = FileUtils.readLines(new File(tweetExtractorPath + "/" + topicBaseFile + ".topic"));
                if (!lines.isEmpty()) {
                    topic = lines.get(0).replaceAll("\"", "");
                }

                List<String> urls = FileUtils.readLines(new File(tweetExtractorPath + "/" + topicBaseFile + ".tweets"));

                String tweetsFile = tweetExtractorPath + "/" + topicBaseFile + ".tweets";
                CollectedDataset collectedDataset = new CollectedDataset(tweetsFile);

                tweetSummarizer = new TweetSummarizer(topic, collectedDataset, urls);
                String summary = tweetSummarizer.getSummary();
                System.out.println("The summary: " + summary);
                System.out.println("Represemtative Tweets: ");
                System.out.println(StringUtils.join(tweetSummarizer.getRepresentativeTweets(), "\n"));
            } catch (IOException ex) {
                System.out.println("ERROR: "+ex.getMessage());
            }

        }
    }

}
