/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.posttokenize.NoiseWords;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author intelWorX
 */
public class NgramExtractor {

    //public static final Pattern EXCLUDE_PATTERN = Pattern.compile("([()?!*.;:=+,\"]+)|(#[a-zA-Z0-9_-]+)|(@[a-zA-Z0-9_]{1,20})");
    public static final Pattern EXCLUDE_PATTERN = Pattern.compile("([()?!*.;:=+,\"]+)|(@[a-zA-Z0-9_]{1,20})");

    public static List<Ngram> extract(List<String> tokens, int n) {
        return extract(tokens, n, true);
    }

    public static List<Ngram> extract(List<String> tokens, int n, boolean filter) {
        int length = tokens.size();
        ArrayList<Ngram> ngrams = new ArrayList<>();
        if (n > 0) {
            for (int i = 0; i <= (length - n); i++) {//8
                String ngram[] = new String[n];
                boolean complete = true;
                for (int j = 0; j < n; j++) {
                    //0-1
                    int k = i + j;
                    String word = tokens.get(k).trim();
                    //is a punctuation symbol
                    //is a hash tag.
                    //is a username or contains username
                    if (filter && shouldExclude(word, n, j)) {
                        complete = false;
                        break;
                    }
                    ngram[j] = word;
                }

                if (complete) {
                    ngrams.add(new Ngram(ngram));
                }
            }
        }
        return ngrams;
    }

    public static boolean shouldExclude(String word, int n, int pos) {
        return EXCLUDE_PATTERN.matcher(word).find() //predefined exclusions
                || ((pos == 0 || word.length() == 1) && word.matches("^[^A-Za-z0-9#]"))
                //group can't start with punctuation
                || (n == 1 && NoiseWords.isNoise(word));
    }
}
