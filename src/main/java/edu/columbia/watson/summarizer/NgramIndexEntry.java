/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.doc.TweetLine;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author intelWorX
 */
public class NgramIndexEntry extends HashSet<TweetLine> {

    final protected HashMap<String, Integer> labels = new HashMap<>();
    protected String maxAct;
    protected int maxActCount = 0;

    public HashMap<String, Integer> getLabels() {
        return labels;
    }

    public int addAct(String label) {
        if (label == null) {
            return 0;
        }
        Integer actCount = labels.get(label);
        if (actCount == null) {
            actCount = 0;
        }
        actCount = actCount + 1;
        if (actCount > maxActCount) {
            maxActCount = actCount;
            maxAct = label;
        }
        labels.put(label, actCount);
        return actCount;
    }

    @Override
    public boolean add(TweetLine e) {
        if (e != null && e.getLabel() != null) {
            addAct(e.getLabel());
        }
        return super.add(e); //To change body of generated methods, choose Tools | Templates.
    }

    public String getMaxAct() {
        return maxAct;
    }

}
