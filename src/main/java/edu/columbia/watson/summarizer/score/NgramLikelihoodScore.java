/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer.score;

import edu.columbia.watson.Indexer;
import edu.columbia.watson.corpus.PriorCorpus;
import edu.columbia.watson.summarizer.Ngram;
import edu.columbia.watson.summarizer.TweetIndexer;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class NgramLikelihoodScore {

    final protected List<Ngram> ngrams;
    final protected TweetIndexer indexer;
    final protected static PriorCorpus priorCorpus = PriorCorpus.instance();
    final protected double LIDSTONE_COEF = 0.5;
    final protected static int priorCorpusWordCount = priorCorpus.keySet().size();
    final protected static int priorCorpusTotalCount = priorCorpus.getTotalCount();

    public NgramLikelihoodScore(List<Ngram> ngrams, TweetIndexer indexer) {
        this.ngrams = ngrams;
        this.indexer = indexer;
    }

    public NgramLikelihoodScore score() {
        for (Ngram ng : ngrams) {
            //double score = computeScoreMotifWay(ng);
            double score = computeScore(ng);
            ng.setLikelihoodScore(score);
        }

        Collections.sort(ngrams, new Comparator<Ngram>() {
            @Override
            public int compare(Ngram o1, Ngram o2) {
                return -1 * ((Double) o1.getLikelihoodScore()).compareTo(o2.getLikelihoodScore());
            }
        });
        return this;
    }

    public List<Ngram> getNgrams() {
        return ngrams;
    }

    public double computeScoreMotifWay(Ngram ngram) {
        double prPhraseGivenSet = computeLikelihoodInCorpus(indexer, ngram.getWords());
        double prPhraseGivenGeneral = computeLikelihoodInCorpus(priorCorpus, ngram.getWords());
        if (prPhraseGivenGeneral == 0) {
            return Double.NEGATIVE_INFINITY;
        }

        return Math.log(prPhraseGivenSet / prPhraseGivenGeneral);
    }

    public double computeScore(Ngram ngram) {
        if (ngram.length == 1) {
            double denom = priorCorpusTotalCount + priorCorpusWordCount * LIDSTONE_COEF;
            double occurence = priorCorpus.count(ngram.getKey());
            double scoreOccur = (occurence + LIDSTONE_COEF) / denom;
            double scoreNonOccur = (priorCorpusTotalCount - occurence + LIDSTONE_COEF) / denom;
            return Math.log(scoreNonOccur / scoreOccur);
        }

        double numberOfTimesCoOccuring = priorCorpus.countCoOccurence(ngram.getWords());
        double numberOfTimesAllOccuring = priorCorpus.countOccurences(ngram.getWords());

        double dependencePr = (numberOfTimesCoOccuring + LIDSTONE_COEF)
                / (numberOfTimesAllOccuring + LIDSTONE_COEF * ngram.length);

        double independencePr = (numberOfTimesAllOccuring - numberOfTimesCoOccuring + LIDSTONE_COEF) / (numberOfTimesAllOccuring + LIDSTONE_COEF * ngram.length);
        //DebugPrint.println("dependence: ", dependencePr, "independence: ", independencePr);
        return Math.log(independencePr / dependencePr);
    }

    /**
     * Compute Phrase likelihood using Lidstone smoothening
     *
     * @param words
     * @param index
     * @return
     */
    protected double computeLikelihoodInCorpus(Indexer index, String... words) {
        long phraseCnt = index.countCoOccurence(words);
        long phlCount = index.totalCountOfPhrasesOfLength(words.length);
        long phTypeCount = index.totalNumberOfPhrasesOfLength(words.length);

        return (phraseCnt + LIDSTONE_COEF) / (phlCount + phTypeCount * LIDSTONE_COEF + 1);//prevent division by 0.
    }

}
