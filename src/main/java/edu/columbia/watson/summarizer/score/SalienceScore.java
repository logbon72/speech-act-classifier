/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer.score;

import edu.columbia.watson.summarizer.Ngram;
import edu.columbia.watson.summarizer.NgramGraph;
import edu.columbia.watson.summarizer.NgramGraphNode;
import java.util.Collections;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class SalienceScore {

    public static final int ITERATIONS = 20;
    public static final double D = 0.85;

    public static void computeSalienceScores(NgramGraph graph) {
        int graphSize = graph.size();
        for (int i = 1; i <= ITERATIONS; i++) {
            //DebugPrint.println("Running iteration ", i);
            for (NgramGraphNode ngramNode : graph) {
                computeGraphScore(ngramNode, graphSize);
            }
        }

        for (NgramGraphNode ngramNode : graph) {
            Ngram ngram = ngramNode.getNgram();
            ngram.setSalience(ngram.length * ngram.getGraphScore());
        }
        Collections.sort(graph);
    }

    private static void computeGraphScore(NgramGraphNode graphNode, int graphSize) {
        double graphScore = (1 - D) / graphSize;

        if (graphNode.getNgram().getGraphScore() != 0) {
            int totalWeightsInAdj = 0;
            for (int weight : graphNode.getAdj().values()) {
                totalWeightsInAdj += weight;
            }

            double sumScoreByWeights = 0;
            for (NgramGraphNode ngramGraphNode : graphNode.getAdj().keySet()) {
                sumScoreByWeights += ngramGraphNode.getNgram().getGraphScore() * graphNode.edgeWeight(ngramGraphNode) / totalWeightsInAdj;
            }
            graphScore += D * sumScoreByWeights;
        }

        graphNode.getNgram().setGraphScore(graphScore);
    }
}
