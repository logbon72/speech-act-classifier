/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer.score;

import edu.columbia.watson.summarizer.Ngram;
import java.util.Arrays;
import java.util.HashSet;

/**
 *
 * @author intelWorX
 */
public class JaccardSimilarity {

    public static int countUnion(String phrase1, String phrase2) {
        HashSet any = new HashSet<>();
        any.addAll(Arrays.asList(phrase1.split("\\s+")));
        any.addAll(Arrays.asList(phrase2.split("\\s+")));
        return any.size();
    }

    public static int countUnion(Ngram phrase1, Ngram phrase2) {
        return countUnion(phrase1.getKey(), phrase2.getKey());
    }

    public static int countIntersection(Ngram phrase1, Ngram phrase2) {
        return countIntersection(phrase1.getKey(), phrase2.getKey());
    }

    public static int countIntersection(String phrase1, String phrase2) {
        HashSet any = new HashSet<>();
        any.addAll(Arrays.asList(phrase1.split("\\s+")));
        any.retainAll(Arrays.asList(phrase2.split("\\s+")));
        return any.size();
    }

    public static double score(Ngram phrase1, Ngram phrase2) {
        return score(phrase1.getKey(), phrase2.getKey());
    }

    public static double score(String phrase1, String phrase2) {
        if (phrase1.isEmpty() || phrase2.isEmpty()) {
            return 0.0;
        } else if (phrase1.equals(phrase2)) {
            return 1.0;
        } else {
            double n = countIntersection(phrase1, phrase2);
            if (n != 0) {
                return n / countUnion(phrase1, phrase2);
            }
            return 0;
        }

    }

}
