/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.summarizer;

import edu.columbia.watson.Indexer;
import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.python.HashtagSplitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class TweetIndexer extends HashMap<Ngram, NgramIndexEntry> implements Indexer<Ngram> {

    public static final int MAX_NGRAM_LENGTH = 6;
    protected List<TweetLine> tweets;
    protected String topic;
    private final HashMap<Integer, Integer> PH_OF_LENGTH = new HashMap<>(MAX_NGRAM_LENGTH);
    private final HashMap<Integer, Integer> PH_OF_LENGTH_N_TOTAL_COUNT = new HashMap<>(MAX_NGRAM_LENGTH);

    public TweetIndexer(List<TweetLine> tweets, String topic) {
        this.tweets = tweets;
        this.topic = topic.startsWith("#") ?  HashtagSplitter.getInstance().split(topic) : topic;
        for (int i = 1; i <= MAX_NGRAM_LENGTH; i++) {
            PH_OF_LENGTH.put(i, 0);
            PH_OF_LENGTH_N_TOTAL_COUNT.put(i, 0);
        }
    }

    //should be able to return all phrases/words
    public Set<Ngram> allNgrams() {
        return keySet();
    }

    //should be able to check if two phrases occur in the same document and the number of times 
    //that happens
    public int countCoOccurence(Ngram ngram1, Ngram ngram2) {
        NgramIndexEntry indexEntry1 = findEntry(ngram1);

        if (ngram1.equals(ngram2)) {
            return indexEntry1.size();
        }

        NgramIndexEntry indexEntry2 = findEntry(ngram2);
        NgramIndexEntry retained = new NgramIndexEntry();

        if (indexEntry1.isEmpty() || indexEntry2.isEmpty()) {
            //intersecition of empty set is an empty set
            return 0;
        }

        retained.addAll(indexEntry1);
        retained.retainAll(indexEntry2);

        return retained.size();
    }

    @Override
    public void index(TweetLine tweet) {
        List<String> tokens = tokenizeTweet(tweet);
        for (int n = 1; n <= Math.min(MAX_NGRAM_LENGTH, tokens.size()); n++) {
            List<Ngram> ngrams = filteredNgrams(NgramExtractor.extract(tokens, n));
            //List<Ngram> ngrams = (NgramExtractor.extract(tokens, n));
            //DebugPrint.println("Ngrams count: ", ngrams.size());
            for (Ngram ngram : ngrams) {
                NgramIndexEntry ne = findEntry(ngram);
                ne.add(tweet);
                //increment count of phrases with that length
                PH_OF_LENGTH_N_TOTAL_COUNT.put(ngram.length, PH_OF_LENGTH_N_TOTAL_COUNT.get(ngram.length) + 1);
                if (ne.size() == 1) {
                    PH_OF_LENGTH.put(ngram.length, PH_OF_LENGTH.get(ngram.length));
                }
            }
        }
    }

    //ability to list the other phrases that occur in the same document as a phrase
    //move to graph
    public TweetIndexer build() {
        if (tweets != null) {
            for (TweetLine tweet : tweets) {
                index(tweet);
            }
        }
        return this;
    }

    /**
     * Find an Ngrams entry
     *
     * @param ngram
     * @return
     */
    public NgramIndexEntry findEntry(Ngram ngram) {
        NgramIndexEntry indexEntry = get(ngram);
        if (indexEntry == null) {
            indexEntry = new NgramIndexEntry();
            put(ngram, indexEntry);
        }
        return indexEntry;
    }
    
    public NgramIndexEntry findEntry(String word) {
        Ngram ngram = new Ngram(new String[]{word});
        return findEntry(ngram);
    }

    public List<String> tokenizeTweet(TweetLine tweetLine) {
        return tweetLine.getDoc();
    }

    /**
     * Filter out unneeed Ngrams
     *
     * @todo check for stop words, ensure topic is not added to list
     *
     * @param ngrams
     * @return
     */
    public List<Ngram> filteredNgrams(List<Ngram> ngrams) {

//        String topicLcase = topic.toLowerCase();
//        Iterator<Ngram> ngramIt = ngrams.iterator();
//        while (ngramIt.hasNext()) {
//            Ngram ngramObj = ngramIt.next();
//            String ngram = ngramObj.getKey();
//            if ((topicLcase.length() > 0 && ngram.contains(topicLcase))) {
//                ngramIt.remove();
//            }
//        }
        return ngrams;
    }

    /**
     * Build graph from list of ngrams
     *
     * @param ngrams
     * @return
     */
    public NgramGraph buildGraph(List<Ngram> ngrams) {
        NgramGraph graph = new NgramGraph();
        for (Ngram ngram : ngrams) {
            graph.addNode(ngram);
        }

        for (NgramGraphNode sourceNode : graph) {
            for (NgramGraphNode targetNode : graph) {
                Ngram sourceNgram = sourceNode.getNgram();
                if (!sourceNode.equals(targetNode)) {
                    //Undirected graph, save cost of building, if edge exists from target to source
                    int coOccurence = targetNode.edgeWeight(sourceNode);
                    if (coOccurence > 0) {
                        sourceNode.adj.put(targetNode, coOccurence);
                    } else {
                        coOccurence = countCoOccurence(sourceNgram, targetNode.getNgram());
                        if (coOccurence > 0) {
                            sourceNode.adj.put(targetNode, coOccurence);
                        }
                    }
                }
            }
        }

        return graph;
    }

    @Override
    public int count(Ngram t) {
        return findEntry(t).size();
    }

    @Override
    public int totalCountOfPhrasesOfLength(int l) {
        Integer c = PH_OF_LENGTH_N_TOTAL_COUNT.get(l);
        return c == null ? 0 : c;
    }

    @Override
    public int totalNumberOfPhrasesOfLength(int l) {
        Integer c = PH_OF_LENGTH.get(l);
        return c == null ? 0 : c;
    }

    public String getAct(Ngram ngram) {
        return findEntry(ngram).getMaxAct();
    }

    @Override
    public int countCoOccurence(String... words) {
        NgramIndexEntry retained = new NgramIndexEntry();
        boolean isFirst = true;
        for (String word : words) {
            if (!NgramExtractor.shouldExclude(word, 1, 0)) {
                if (isFirst) {
                    retained.addAll(findEntry(word.toLowerCase()));
                    isFirst = false;
                } else if (retained.isEmpty()) {
                    return 0;
                } else {
                    retained.retainAll(findEntry(word.toLowerCase()));
                }
            }
        }

        return retained.size();
    }

    @Override
    public int countOccurences(String... words) {
        NgramIndexEntry union = new NgramIndexEntry();
        for (String word : words) {
            if (!NgramExtractor.shouldExclude(word, 1, 0)) {
                union.addAll(findEntry(word.toLowerCase()));
            }
        }
        return union.size();
    }
}
