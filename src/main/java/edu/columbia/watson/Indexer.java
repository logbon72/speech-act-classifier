/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson;

import edu.columbia.watson.doc.TweetLine;

/**
 *
 * @author intelWorX
 * @param <T>
 */
public interface Indexer<T> {
    public int count(T t);
    public void index(TweetLine t);
    public int totalCountOfPhrasesOfLength(int l);
    public int totalNumberOfPhrasesOfLength(int l);
    public int countCoOccurence(String... words);
    public int countOccurences(String... words);
}
