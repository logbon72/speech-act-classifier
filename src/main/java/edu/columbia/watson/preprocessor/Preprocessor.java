/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor;

/**
 *
 * @author intelWorX
 */
abstract public class Preprocessor {

    public static final String USERNAME_CLASS = "_USERN_";
    public static final String EMOTICON_CLASS = "";
    public static final String CLASS_QUERY_TERM = "_QUERY_TERM_";
    public static final String CLASS_URL = "_URL_";

    protected boolean forTraining = true;
    protected boolean forTesting = true;

    abstract public String process(String inputTweet, String queryTerm);

    public boolean isForTraining() {
        return forTraining;
    }

    public boolean isForTesting() {
        return forTesting;
    }

}
