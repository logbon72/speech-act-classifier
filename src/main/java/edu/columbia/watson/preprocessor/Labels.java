/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor;

/**
 *
 * @author intelWorX
 */
public enum Labels {

    POSITIVE("4"),
    NEGATIVE("0"),
    NEUTRAL("2"),;
    public final String label;

    private Labels(String lb) {
        label = lb;
    }
}
