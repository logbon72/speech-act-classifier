/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.preprocessor.list;

import edu.columbia.watson.preprocessor.Preprocessor;

/**
 *
 * @author intelWorX
 */
public class HashTagsToTerm extends Preprocessor{

    @Override
    public String process(String inputTweet, String queryTerm) {
        return inputTweet.replaceAll("#([a-zA-Z_0-9-]+)", "$1");
    }
    
}
