/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor.list;

import edu.columbia.watson.preprocessor.Preprocessor;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author intelWorX
 */
public class UnEscapeHtmlEntities extends Preprocessor {

    @Override
    public String process(String inputTweet, String queryTerm) {
        return StringEscapeUtils.unescapeHtml(inputTweet);
    }

}
