/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor.list;

import edu.columbia.watson.preprocessor.Preprocessor;
import java.util.regex.Pattern;

/**
 *
 * @author intelWorX
 */
public class ReplaceURL extends Preprocessor {

    public static final Pattern p = Pattern.compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

    @Override
    public String process(String inputTweet, String queryTerm) {
        return inputTweet.replaceAll(p.pattern(), CLASS_URL);
    }

}
