/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor.list;

import edu.columbia.watson.preprocessor.Preprocessor;
import java.util.regex.Pattern;

/**
 *
 * @author intelWorX
 */
public class MaskQueryTerm extends Preprocessor {

    public MaskQueryTerm() {
        forTraining = false;
    }

    @Override
    public String process(String inputTweet, String queryTerm) {
        queryTerm = Pattern.quote(queryTerm.replaceAll("^#+", ""));
        return inputTweet.replaceAll("(?i)(#?" + queryTerm + ")", CLASS_QUERY_TERM);
    }

}
