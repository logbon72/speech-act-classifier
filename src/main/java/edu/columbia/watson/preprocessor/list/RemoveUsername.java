/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.columbia.watson.preprocessor.list;

import edu.columbia.watson.preprocessor.Preprocessor;

/**
 *
 * @author intelWorX
 */
public class RemoveUsername extends Preprocessor{

    
    @Override
    public String process(String inputTweet, String q) {
        return inputTweet.replaceAll("@[a-zA-Z0-9_]{1,15}", USERNAME_CLASS);
    }
    
}
