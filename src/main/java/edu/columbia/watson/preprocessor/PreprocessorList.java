/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.preprocessor;

import edu.columbia.watson.preprocessor.list.HashTagsToTerm;
import edu.columbia.watson.preprocessor.list.LowerCaseTweet;
import edu.columbia.watson.preprocessor.list.MaskQueryTerm;
import edu.columbia.watson.preprocessor.list.RemoveEmoticons;
import edu.columbia.watson.preprocessor.list.RemoveUsername;
import edu.columbia.watson.preprocessor.list.ReplaceRepeatedLetters;
import edu.columbia.watson.preprocessor.list.ReplaceURL;
import edu.columbia.watson.preprocessor.list.UnEscapeHtmlEntities;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author intelWorX
 */
public class PreprocessorList extends ArrayList<Preprocessor> {

    public static final int PROCESS_TRAINING = 1;
    public static final int PROCESS_TESTING = 2;
    private static final PreprocessorList instance = new PreprocessorList();

    public static PreprocessorList instance() {
        if (instance.isEmpty()) {
            init();
        }
        return instance;
    }

    public static void init() {
        instance.add(new LowerCaseTweet());
        instance.add(new UnEscapeHtmlEntities());
        instance.add(new RemoveEmoticons());
        instance.add(new ReplaceURL());
        instance.add(new RemoveUsername());
        instance.add(new HashTagsToTerm());
        instance.add(new MaskQueryTerm());
        instance.add(new ReplaceRepeatedLetters());
    }

    public static String preProcess(String tweet, String queryTerm, int flag, HashSet<String> preprocessors) {
        String outTweet = tweet;
        for (Preprocessor preprocessor : instance()) {
            if (preprocessors.isEmpty() || preprocessors.contains(preprocessor.getClass().getSimpleName())) {
                if (((flag & PROCESS_TESTING) != 0 && preprocessor.isForTesting())
                        || ((flag & PROCESS_TRAINING) != 0 && preprocessor.isForTraining())) {
                    outTweet = preprocessor.process(outTweet, queryTerm);
                }
            }
        }
        return outTweet;
    }

}
