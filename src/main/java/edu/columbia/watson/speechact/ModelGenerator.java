/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.speechact;

import edu.columbia.watson.Configuration;
import edu.columbia.watson.doc.TweetLine;
import edu.columbia.watson.features.FeatureExtractor;
import edu.columbia.watson.features.FeatureList;
import edu.columbia.watson.features.FeatureType;
import edu.columbia.watson.impl.doc.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DefaultDataset;
import net.sf.javaml.core.SparseInstance;
import net.sf.javaml.tools.data.FileHandler;

/**
 *
 * @author intelWorX
 */
public class ModelGenerator {

    public static void main(String[] args) {
        try {
            FeatureList.INSTANCE.loadFromTsv(Configuration.instance().getProperty("training.features.list", "data/featurelist.tsv"), true);
            //convert training set
            System.out.println("Writing training file");
            SpeechActDataSet trainingSet = new SpeechActTrainnigSet();
            Dataset mlDataset = new DefaultDataset();
            FeatureExtractor fe = new FeatureExtractor(FeatureType.MODE_TRAINING);
            while (trainingSet.hasNext()) {
                TweetLine tl = trainingSet.getNext();
                SparseInstance sparseInstance = FeatureList.getSparseRepresentation(fe.extractAllFeatures(tl.getDoc()), tl.getLabel());
                mlDataset.add(sparseInstance);
            }
            String outFile = Configuration.instance().getProperty("training.model.file", "data/model.data");
            System.out.println("exporting data set to file: " + outFile);
            FileHandler.exportDataset(mlDataset, new File(outFile));
            mlDataset.clear();

            //convert 
            System.out.println("Working on testing file");
            fe = new FeatureExtractor(FeatureType.MODE_TESTING);
            SpeechActDataSet testingDataSet = new SpeechActTestingSource();
            while (testingDataSet.hasNext()) {
                TweetLine t = testingDataSet.getNext();
                SparseInstance sparseInstance = FeatureList.getSparseRepresentation(fe.extractAllFeatures(t.getDoc()), t.getLabel());
                
                mlDataset.add(sparseInstance);
            }

            outFile = Configuration.instance().getProperty("testing.model.file", "data/testing.data");
            System.out.println("exporting data set to file: " + outFile);
            FileHandler.exportDataset(mlDataset, new File(outFile));

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ModelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ModelGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
