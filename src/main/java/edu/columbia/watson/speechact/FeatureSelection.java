/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.speechact;

import edu.columbia.watson.Configuration;
import edu.columbia.watson.doc.*;
import edu.columbia.watson.features.*;
import edu.columbia.watson.impl.doc.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import misc.Log;
import net.sf.javaml.core.*;
import net.sf.javaml.featureselection.FeatureScoring;
import net.sf.javaml.featureselection.scoring.GainRatio;
import net.sf.javaml.featureselection.scoring.KullbackLeiblerDivergence;

/**
 *
 * @author intelWorX
 */
public class FeatureSelection {

    HashMap<String, Integer> featureList = new HashMap<>();
    HashMap<Integer, String> featureListRev = new HashMap<>();
    //ArrayList<DocumentFeatureVector> documents = new ArrayList<>();
    Dataset trainingDataset = new DefaultDataset();
    TreeMap<Integer, Double> featureScores;

    private static void buildOptions() {

    }

    public static void main(String[] args) throws IOException {
        FeatureSelection fe = new FeatureSelection();
        try {
            fe.extractTrainingFeatures();
            System.out.println("total trainig file size: " + fe.trainingDataset.size());
            System.out.println("total feature size: " + fe.featureList.size());
            fe.scoreFeatures();
            String pad = Configuration.instance().getProperty("features.id.pad", "100000");
            String selectionFile = Configuration.instance().getProperty("features.selection.file", "data/selection.tsv");
            System.out.println("Writing to file");
            fe.writeFeatures(selectionFile, Integer.parseInt(pad));
        } catch (IOException ioe) {
            System.out.println("Could not load input file: " + ioe.getLocalizedMessage());
        }
    }

    public FeatureSelection(String pathToConfig) {
        if (pathToConfig != null) {
            Configuration.setConfigPath(pathToConfig);
        }

    }

    public FeatureSelection() {
        this(null);
    }

    public void extractTrainingFeatures() throws IOException {
        SpeechActDataSet trainingSource = new SpeechActTrainnigSet();
        FeatureExtractor featureExtractor = new FeatureExtractor(FeatureType.MODE_TRAINING);
        trainingDataset.clear();
        featureList.clear();
        featureListRev.clear();

        TweetLine tweetLine;
        List<Feature> features;
        SparseInstance dataInstance;

        Log.getLogger().info("Loading training set");
        //get document from tweet
        int maxAttributes = 0;
        //int line = 0;
        while (trainingSource.hasNext()) {
            tweetLine = trainingSource.getNext();
            //System.out.println("Processing Line: " + (++line)+ " c: " + tweetLine.getLabel());
            features = featureExtractor.extractAllFeatures(tweetLine.getDoc());
            dataInstance = new SparseInstance();
            Integer key;
            for (Feature f : features) {
                key = featureList.get(f.getHash());
                if (key == null) {
                    key = featureList.size();
                    featureList.put(f.getHash(), key);
                    featureListRev.put(key, f.getHash());
                }
                dataInstance.put(key, 1.0);
            }
            dataInstance.setClassValue(tweetLine.getLabel());
            if (dataInstance.noAttributes() > maxAttributes) {
                maxAttributes = dataInstance.noAttributes();
            }
            //System.out.println(dataInstance);
            trainingDataset.add(dataInstance);
        }

        int i = 0;
        for (Instance di : trainingDataset) {
            ((SparseInstance) di).setNoAttributes(maxAttributes);
        }

        Log.getLogger().info("Done loading training set");
        System.out.println("Max Attributes: " + maxAttributes);
        //running gain
    }

    public void scoreFeatures() {
        FeatureScoring scorer = new GainRatio();
        /* Apply the algorithm to the data set */
        scorer.build(trainingDataset);
        if (featureScores != null) {
            featureScores.clear();
        }

        final HashMap<Integer, Double> tmpMap = new HashMap<>(scorer.noAttributes());
        /* Print out the score of each attribute */
        System.out.println("Printing out scores, size: " + scorer.noAttributes());
        for (int i = 0; i < scorer.noAttributes(); i++) {
            tmpMap.put(i, scorer.score(i));
        }

        featureScores = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                Double obj1 = tmpMap.get(o1);
                Double obj2 = tmpMap.get(o2);
                if (obj1 == null) {
                    return 1;
                } else if (obj2 == null) {
                    return -1;
                } else {
                    int cmp = obj2.compareTo(obj1);
                    if (cmp != 0) {
                        return cmp;
                    }
                    return o2.compareTo(o1);
                }
            }
        });

        System.out.println("Size of tmpMap: " + tmpMap.size());
        featureScores.putAll(tmpMap);
        //
        System.out.println("Size of featureScore: " + featureScores.size());

    }

    public void writeFeatures(String path, Integer featureIdPad) throws FileNotFoundException {
        File outfile = new File(path);
        try (Formatter fo = new Formatter(outfile)) {
            for (Integer attrId : featureScores.keySet()) {
                fo.format("%d\t%.3f\t%s%n", featureIdPad + attrId, featureScores.get(attrId), featureListRev.get(attrId));
            }
            fo.flush();
        }
    }

}
