/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.speechact;

import edu.columbia.watson.doc.*;
import edu.columbia.watson.features.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.KNearestNeighbors;
import net.sf.javaml.classification.bayes.NaiveBayesClassifier;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.SparseInstance;
import net.sf.javaml.tools.data.FileHandler;

/**
 *
 * This class handles speech act classification.
 *
 * @author intelWorX
 */
public class SpeechActClassifier {
//sta: statement
//com: comment
//que: question
//sug: suggestion
//mis: miscellaneous

    public static final String ACT_STA = "sta";
    public static final String ACT_COM = "com";
    public static final String ACT_QUE = "que";
    public static final String ACT_SUG = "sug";
    public static final String ACT_MISC = "mis";

    private FeatureList featureList;
    private FeatureExtractor featureExtractor;
    private Dataset trainingDataset;
    private Classifier classifier;
    private boolean initialized = false;

    public static final SpeechActClassifier INSTANCE = new SpeechActClassifier();
    //public final static Classifier DEFAULT__CLASSIFIER = new NaiveBayesClassifier(true, true, true);
    public final static Classifier DEFAULT__CLASSIFIER = new KNearestNeighbors(3);

    private SpeechActClassifier() {
        featureExtractor = new FeatureExtractor(FeatureType.MODE_TESTING);
    }

    final public SpeechActClassifier setClassifier(Classifier classifier) {
        this.classifier = classifier;
        if (trainingDataset != null) {
            classifier.buildClassifier(trainingDataset);
        }
        return this;
    }

    public SpeechActClassifier init(String modelFile, String featureFile, Classifier classifier) throws IOException {
        return init(new File(modelFile), featureFile, classifier);
    }

    public SpeechActClassifier init(File modelFile, String featurePath) throws IOException {
        return init(modelFile, featurePath, classifier);
    }

    public SpeechActClassifier init(File model, String featureFile, Classifier classifier) throws IOException {
        trainingDataset = FileHandler.loadSparseDataset(model, 0);
        featureList = FeatureList.INSTANCE;
        featureList.loadFromTsv(featureFile, true);

//set classifier
        if (classifier != null) {
            this.classifier = classifier;
        }

        if (this.classifier == null) {
            this.classifier = DEFAULT__CLASSIFIER;
        }

        setClassifier(classifier);

        initialized = true;
        return this;
    }

    public List<TweetLine> classifyTweets(TweetSource tweetSource) {
        return classifyTweets(tweetSource, 0);
    }

    /**
     * Accepts a tweet source and returns a list of tweets with their label set.
     *
     * @param tweetSource
     * @param limit
     * @return
     */
    public List<TweetLine> classifyTweets(TweetSource tweetSource, int limit) {
        if (!initialized) {
            throw new RuntimeException("The speech act classifier is not yet initialized");
        }

        int count = tweetSource.count();
        ArrayList<TweetLine> classifiedTweets = new ArrayList<>(count > 0 ? count : 1);

        while ((limit < 1 || classifiedTweets.size() < limit) && tweetSource.hasNext()) {
            TweetLine tweetLine = classifyTweet(tweetSource.getNext());
            classifiedTweets.add(tweetLine);
        }

        return classifiedTweets;
    }

    public TweetLine classifyTweet(TweetLine tweetLine) {
        List<Feature> features = featureExtractor.extractAllFeatures(tweetLine.getDoc());
        SparseInstance dataInstance = FeatureList.getSparseRepresentation(features, null);
        Object classLabel = classifier.classify(dataInstance);
        tweetLine.setLabel((String) classLabel);
        return tweetLine;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setFeatureExtractor(FeatureExtractor featureExtractor) {
        this.featureExtractor = featureExtractor;
    }

    public FeatureExtractor getFeatureExtractor() {
        return featureExtractor;
    }

}
