/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.speechact;

import edu.columbia.watson.Configuration;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.KNearestNeighbors;
import net.sf.javaml.classification.bayes.NaiveBayesClassifier;
import net.sf.javaml.classification.evaluation.EvaluateDataset;
import net.sf.javaml.classification.evaluation.PerformanceMeasure;
import net.sf.javaml.classification.tree.RandomForest;
import net.sf.javaml.classification.tree.RandomTree;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.tools.data.FileHandler;

/**
 *
 * @author intelWorX
 */
public class Evaluator {

    public static List<Classifier> getClassifiers() {
        ArrayList<Classifier> classifiers = new ArrayList<>();
        for (int i = 3; i <= 6;i++) {
            classifiers.add(new KNearestNeighbors(i));
        }
        //classifiers.add(new KDtreeKNN(6));
        //classifiers.add(new LibSVM());
        //classifiers.add(new NaiveBayesClassifier(true, true, true));
        //classifiers.add(new WekaClassifier(new SMO()));
        //classifiers.add(new RandomForest(3));
        //classifiers.add(new RandomTree(attributes, null));

        return classifiers;
    }

    public static void main(String[] args) {

        try {
            String pathToTraining = Configuration.instance().getProperty("training.model.file", "data/model.data");
            Dataset trainingDataset = FileHandler.loadSparseDataset(new File(pathToTraining), 0);

            String pathToTesting = Configuration.instance().getProperty("testing.model.file", "data/testing.data");
            Dataset testingDataset = FileHandler.loadSparseDataset(new File(pathToTesting), 0);

//            Classifier knn = new KNearestNeighbors(5);
//            knn.buildClassifier(trainingDataset);
            List<Classifier> classifiers = getClassifiers();
            //classifiers.add(new RandomTree(trainingDataset.noAttributes(), new Random()));

            for (Classifier cn : classifiers) {
                System.out.println("Classifying using : " + cn.getClass().getCanonicalName());
                cn.buildClassifier(trainingDataset);
                Map<Object, PerformanceMeasure> pm = EvaluateDataset.testDataset(cn, testingDataset);
                double total = 0;
                System.out.println("Prining evaluation.");
                for (Object o : pm.keySet()) {
                    double curr = pm.get(o).getFMeasure();
                    System.out.println(o + ": " + curr);
                    total += curr;
                }

                System.out.println("AVG : " + total / pm.size());
            }

        } catch (IOException ex) {
            Logger.getLogger(Evaluator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
