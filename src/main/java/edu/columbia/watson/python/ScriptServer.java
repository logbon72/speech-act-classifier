/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.python;

import edu.columbia.watson.Configuration;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author intelWorX
 */
abstract public class ScriptServer {

    protected Process pythonScript;

    public static final String EOL = String.format("%n");
    
    public ScriptServer(String scriptPath, String... params) throws IOException {
        //Process process = new ProcessBuilder("C:\\PathToExe\\MyExe.exe","param1","param2").start();
        ArrayList<String> commands = new ArrayList<>(params.length + 2);
        commands.add(Configuration.instance().getProperty("python.exec.path", "/usr/bin/python"));
        commands.add(scriptPath);
        if (params.length > 0) {
            commands.addAll(Arrays.asList(params));
        }
        ProcessBuilder processBuilder = new ProcessBuilder(commands);
        processBuilder.redirectErrorStream(true);
        pythonScript = processBuilder.start();
    }

    /**
     * Read output from script by piping content in.
     *
     * @param content
     * @return
     * @throws java.io.IOException
     */
    public String pipeInput(String content) throws IOException {
        //OutputStream processInputStream = pythonScript.getOutputStream();
        //InputStream processOutputStream = pythonScript.getInputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(pythonScript.getOutputStream()));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(pythonScript.getInputStream()));
        final StringBuilder outString = new StringBuilder();

        String[] lines = content.split("[\\r\\n]+");
        String line;

        for (String inLine  : lines) {
            writer.write(inLine);
            writer.write(EOL);
//            Unccomment for large in puts
//            if (inLine.length() == 0) {
//                DebugPrint.println("Flushing...");
//                writer.flush();
//            }
        }
//        DebugPrint.println("Flushing...");
        writer.flush();
        
        while ((line = reader.readLine()) != null) {
//            System.out.println("line: "+line);
            outString.append(line).append(EOL);
            if (line.length() == 0) {
                break;
            }
        }

        return outString.toString();
    }

    public void terminate() {
        pythonScript.destroy();
    }
}
