/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.columbia.watson.python;

import edu.columbia.watson.Configuration;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import misc.DebugPrint;

/**
 *
 * @author intelWorX
 */
public class HashtagSplitter extends ScriptServer {

    private double lastConfidence;
    final private HashMap<String, String> cache = new HashMap<>();
    private static HashtagSplitter instance;

    private HashtagSplitter() throws IOException {
        super(Configuration.instance().getProperty("python.scripts.splitter", "../hashtag-segmentation/splitter.py"));
    }

    public static HashtagSplitter getInstance() {
        if (instance == null) {
            try {
                instance = new HashtagSplitter();
            } catch (IOException ex) {
                Logger.getLogger(HashtagSplitter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return instance;
    }

    public String split(String hashtag) {
        try {
            String cachedResult = cache.get(hashtag.toLowerCase());
            if (cachedResult != null) {
                return cachedResult;
            }

            String[] op = pipeInput(hashtag + EOL).split("[\\t\\r\\n]+");
            if (op.length > 1) {
                String result = op[0];
                cache.put(hashtag.toLowerCase(), result);
                //DebugPrint.println("Looking up ", hashtag);
                lastConfidence = Double.parseDouble(op[1]);
                return result;
            }
            return null;
        } catch (IOException ex) {
            Logger.getLogger(HashtagSplitter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public double getLastConfidence() {
        return lastConfidence;
    }

    public static int replaceHashTags(List<String> tokens) {
        int replaced = 0;
        getInstance();
        for (int i = 0; i < tokens.size(); i++) {
            if (tokens.get(i).startsWith("#")) {
                //replace hash tag.
                String replc = instance.split(tokens.get(i));
                if (replc != null) {
                    tokens.set(i, instance.split(tokens.get(i)));
                    replaced++;
                }
            }
        }
        return replaced;
    }
}
