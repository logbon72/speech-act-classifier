/**
 * %SVN.HEADER%
 */
package net.sf.javaml.filter.instance;

import java.util.Arrays;
import java.util.HashSet;
import net.sf.javaml.core.Instance;
import net.sf.javaml.filter.InstanceFilter;

public class ReplaceValueFilter implements InstanceFilter {

    HashSet<Double> from = new HashSet<>();
    Double to;

    public ReplaceValueFilter(Double from, Double to) {
        this.from.add(from);
        this.to = to;
    }

    public ReplaceValueFilter(Double[] from, Double to) {
        this.from.addAll(Arrays.asList(from));
        this.to = to;
    }

    @Override
    public void filter(Instance inst) {
        for (Integer i : inst.keySet()) {
            if (from.contains(inst.get(i))) {
                inst.put(i, to);
            }
        }

    }

}
