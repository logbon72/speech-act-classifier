package net.sf.javaml.featureselection.scoring;

import be.abeel.util.HashMap2D;
import java.util.HashMap;
import misc.DebugPrint;

import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.Instance;
import net.sf.javaml.featureselection.FeatureScoring;
import net.sf.javaml.filter.normalize.NormalizeMidrange;

/**
 * Feature scoring algorithm based on Kullback-Leibler divergence of the value
 * distributions of features.
 *
 *
 * Note: Calling the build method will normalize the data.
 *
 * @author Thomas Abeel
 *
 */
public class KullbackLeiblerDivergence implements FeatureScoring {

    private double[] maxDivergence;

    private HashMap2D<Object, Object, double[]> pairWiseDivergence = new HashMap2D<Object, Object, double[]>();

    private int bins;

    HashMap<String, HashMap<Integer, Double>> computedCounts = new HashMap<>();

    public KullbackLeiblerDivergence() {
        this(100);
    }

    public KullbackLeiblerDivergence(int i) {
        this.bins = i;
    }

    private void computeCounts(Dataset data) {

        for (int i = 0; i < data.noAttributes(); i++) {
            for (Object cl : data.classes()) {
                String k = cl.toString() + i;
                HashMap<Integer, Double> binCounts = new HashMap<>();
                for (int j = 0; j < bins; j++) {
                    binCounts.put(j, 0.0);
                }

                for (Instance inst : data) {
                    if (inst.classValue().equals(cl)) {
                        int idx = (int) inst.value(i);
                        Double binCount = binCounts.get(idx);
                        if (binCount == null) {
                            binCount = 0.0;
                        }
                        binCount += 1.0;
                        binCounts.put(idx, binCount);
                    }
                }

                computedCounts.put(k, binCounts);
            }
        }
    }

    @Override
    public void build(Dataset data) {
        maxDivergence = new double[data.noAttributes()];
        /* Normalize to [0,100[ */
        NormalizeMidrange nm = new NormalizeMidrange(bins / 2, bins - 0.000001);
        nm.build(data);
        nm.filter(data);

        computeCounts(data);
        /* Calculate all pairwise divergencies */
        for (Object p : data.classes()) {
            for (Object q : data.classes()) {
                if (!p.equals(q)) {
                    double[] d = pairWise(p, q, data);
                    pairWiseDivergence.put(p, q, d);
                }
            }
        }
        
        computedCounts.clear();
        /* Search for maximum pairwise divergencies */
        for (Object p : data.classes()) {
            for (Object q : data.classes()) {
                double[] d = pairWiseDivergence.get(p, q);
                if (d != null) {
                    for (int i = 0; i < d.length; i++) {
                        if (d[i] > maxDivergence[i]) {
                            maxDivergence[i] = d[i];
                        }
                    }
                }
            }
        }

    }

    private double[] pairWise(Object p, Object q, Dataset data) {
        double[] divergence = new double[data.noAttributes()];
        /*
         * For probability distributions P and Q of a discrete random variable
         * the K-L divergence of Q from P is defined to be:
         * 
         * D_KL(P|Q)=sum_i(P(i)log(P(i)/Q(i)))
         */
        double maxSum = 0;
        for (int i = 0; i < data.noAttributes(); i++) {
            double sum = 0;
            //double[] countQ = new double[bins];
            //double[] countP = new double[bins];
            double pCount = 0, qCount = 0;
            String pKey = p.toString() + i;
            HashMap<Integer, Double> countP = computedCounts.get(pKey);
            String qKey = q.toString() + i;
            HashMap<Integer, Double> countQ = computedCounts.get(qKey);
            for (Double c : countP.values()) {
                pCount += c;
            }

            for (Double c : countQ.values()) {
                qCount += c;
            }

            for (int j = 0; j < bins; j++) {
                countP.put(j, countP.get(j) / pCount);
                countQ.put(j, countQ.get(j) / qCount);
                /*
                 * Probabilities should never be really 0, they can be small
                 * though
                 */
                if (countP.get(j) == 0) {
                    countP.put(j, 0.0000001);
                }

                if (countQ.get(j) == 0) {
                    countQ.put(j, 0.0000001);
                }
                sum += countP.get(j) * Math.log(countP.get(j) / countQ.get(j));
            }
            divergence[i] = sum;
            /* Keep track of highest value */
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
        /* Normalize to [0,1] */
        for (int i = 0; i < data.noAttributes(); i++) {
            divergence[i] /= maxSum;
        }
        return divergence;
    }

    @Override
    public double score(int attribute) {
        return maxDivergence[attribute];
    }

    @Override
    public int noAttributes() {
        return maxDivergence.length;
    }

}
