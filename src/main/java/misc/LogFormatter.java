/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

/**
 *
 * @author intelWorX
 */
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        return "[" + new Date(record.getMillis()) + "]\t"
                //+ record.getSourceClassName() + "::"
                //+ record.getSourceMethodName() + "::"
                + record.getMessage() + "\n";
    }
}