/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author intelWorX
 */
public class Log {

    protected static final Logger logger = Logger.getLogger(Log.class.getName());
    protected static boolean initted = false;
    private static void init() {
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.ALL);
        Handler fileHandler;
        try {
            fileHandler = new FileHandler("logger.log", true);
            fileHandler.setFormatter(new LogFormatter());
            logger.addHandler(fileHandler);
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
        initted = true;
    }

    public static Logger getLogger() {
        if(!initted){
            init();
        }
        return logger;
    }
    
    
}
