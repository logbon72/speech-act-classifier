*$	Starbucks
*$$	Starbucks
10m	Ten man
10q	Thank you
1ce	Once
2	To
22	Tutu
25m	Twenty-five man
26y4u	Too sexy for you
2ZDA	Tuesday
2day	Today
2l8	Too late
2moro	Tomorrow
2morrow	Tomorrow
2moz	Tomorrow
2mr	Tomorrow
2mrw	Tomorrow
2nite	Tonight
2nt	Tonight
3RZDA	Thursday
3arc	Treyarch
3q	Thank you
3sum	Threesome
4ever	Forever
4get	Forget
4gm	Forgive me
4got	Forgot
4n	Foreign
4nr	Foreigner
4rl	For real
4sale	For sale
4sho	For sure
4u	For you
4ward	Forward
6y	Sexy
AM	Antemeridian
Attny	Attorney
BD	Blu-ray
BF	B.F. Sword
BK	Back
BR	Bathroom
BR	Blu-ray
BRBRB	Bathroom, be right back
Bennifer	Ben and Jennifer
Blizz	Blizzard
Brangelina	Brad and Angelina
Brillz	Brilliant
Bro	Brother
CD9	Parents are watching
CD99	Parents are no longer watching
CX	Correction
Chingo	Chat lingo
Churrin	Children
Dece	Decent
Dweet	Drunk tweet
EPM	Effective actions per minute
EPO	Erythropoietin
Elo	Elo rating system
Exp	Experience
G9	Genius
G9	Goodnight
ID10T	Idiot
IGN	In-game name
IHA	I hate acronyms
IONO	I don't know
IR8	Irate
JLo	Jennifer Lopez
K	Kiss
L	Laughing
L8	Late
L8rg8r	Later gator
LI	LinkedIn
M/F	Male or female
MS	Microsoft
MT	Mistype
MYB	myYearbook
McD	McDonald's
Ms	Miss
NP	Neopets
Nal	Nationality
OO	OpenOffice
OT	Overtime
PL&	Planned
PM	Postmeridian
Permabanned	Permanently banned
Pl	Playlist
RT	Retweet
Rm	Room
Rx	Prescription
SO8	Sweet
Sched	Schedule
Smishing	SMS phishing
Spacing	Spacing out
Sry	Sorry
TM	Trademark
TRBL	Terrible
Table	Turntable
Tix	Tickets
Tlkin	Talking
Tlking	Talking
Tots	Tater tots
Tweeps	Twitter peeps
ULKGR8	You look great
UR	Your
URGR8	You are great
Undies	Underwear
VM	Voicemail
Veggies	Vegetables
Vet	Veteran
Vet	Veterinarian
W8	Wait
WOL	Wings of Liberty
W^	What's up
Wannabe	Want to be
Webpany	Web Company
Wha	What
XO	Kiss and Hug
Y	Why
Y	YMCA
Y	Yawning
Yr	Your
Z	Zero
ZZZ	Sleeping
aa	Autoattack
aar8	At any rate
abbrev	Abbreviation
abt	About
abt2	About to
ack	Acknowledged
add	Address
adds	Additional mobs
addy	Address
admin	Administrator
aggro	Aggression
agi	Agility
agl	Angel
aight	Alright
aipf	All in preflop
ak	AK-47
alt	A lot
alt	Alternate character
amazn	Amazing
ammo	Ammunition
anon	Anonymous
app	Application
app	Appreciate
arp	Armor penetration
arpen	Armor penetration
attn	Attention
awks	Awkward
b	Be
b-day	Birthday
b/c	Because
b/f	Boyfriend
b/f	Brain fart
b/r	Bathroom
b/w	Between
b4	Before
b4u	Before you
bae	Babe
bb/100	Big bets per 100 hands
bbq	Barbecue
bc	Because
bcos	Because
bcoz	Because
bcz	Because
bd	Backdoor
bd	Birthday
bday	Birthday
belf	Blood Elf
bf	Boyfriend
bg	Battleground
bhl8	Be home late
bibi	Bye bye
biz	Business
bling	Baneling
blops	Black Ops
blvmot	Believe me on that
bod	Body
bot	Bottom lane
bout	About
boyds	Back off you dipstick
brd	Bored
brez	Battle resurrection
broom	Bathroom
bstd	Busted
bt	BitTorrent
bt	Bluetooth
bthrm	Bathroom
btwn	Between
bup	Backup Plan
bz	Busy
c	See
c-p	Sleepy
c/p	Crossposting
cata	Cataclysm
cd	Cooldown
cdr	Cooldown reduction
cell	Cell phone
char	Character
chargelot	Charge Zealot
chk	Check
chln	Chilling
chx	Chickens
cia	See ya
cld	Could
clk	Click
cmon	Come on
combo	Combination
comp	Computer
convo	Conversation
coo	Cool
cre8	Create
crit	Critical Strike
crz	Crazy
ctr	Center
cu2mr	See you tomorrow
cud	Could
cul8r	Call you later
cul8r	See you later
cul8rm8	See you later m8
cuz	Because
cya	See ya
cyal8r	See you later
cyr	Call your
cz	Because
d/c	Disconnected
d/l	Download
d8	Date
dal	Dalaran
dc	Daycare
dc	Disconnect
dced	Disconnected
de	Disenchant
deagle	Desert Eagle
ded	Dead
deeps	Damage per second
deets	Details
def	Defense
def	Definitely
delish	Delicious
dept	Department
dfg	Deathfire Grasp
disc	Discipline
dl	Download
dlc	Downloadable content
dmg	Damage
dnbl8	Do not be late
dno	Don't know
dnt	Don't
dom	Dominate
doo	Dude
dq	Disqualify
dstr8	Dang straight
e-ok	Electronically OK
e1	Everyone
eSports	Electronic sports
ea	Each
em	E-mail
ema	E-mail address
emo	Emotional
emsg	E-mail Message
enuf	Enough
essench	Essential
every1	Everyone
ez	Easy
ezpz	Easy peasy
ezy	Easy
f	Female
fab	Fabulous
fav	Favorite
fave	Favorite
favs	Favorites
fb	Facebook
fb	Feedback
fbc	Facebook Chat
fbf	Facebook friend
feat	Featuring
feds	Federal agents
fn	Fine
fo sho	For sure
frnd	Friend
fro	Afro
ftr	After
fw	Freeware
fwd	Forward
g	Grin
g/f	Girlfriend
g8r	Gator
gb	Gigabyte
gb	Goodbye
gf	Girlfriend
ggs	Good games
gibs	Giblets
git	Get over it
gm	Grandmaster
gma	Grandma
gn	Goodnight
gnight	Good night
gnite	Goodnight
gorge	Gorgeous
gpw	Global Password
gr8	Great
grats	Congratulations
gratz	Congratulations
grl	Girl
gtrdun	Get 'er done
gv	Give
gvn	Given
gz	Congratulations
h/w	Homework
h4x0rz	Hackers
h8	Hate
h8r	Hater
h8t	Hate
hav	Have
hax	Hacks
hbd	Happy birthday
hi5	High five
hlp	Help
hm	Home
hmp	Humph
hmwk	Homework
holla	Holler
hon	Honey
hood	Neighborhood
horribad	Horribly bad
hp	Handphone
hp5	Health per five seconds
hr	Home run
hr	Homeroom
hry	Hurry
hun	Honey
hw	Hardware
hw	Homework
hwk	Homework
icc	Icecrown Citadel
icurnvs	I see you are envious
idd	Indeed
ight	Alright
ima	I'm going to
imba	Imbalanced
imot	I'm on that
inb4	In before
inc	Incoming
indie	Independent
indy	Independent
indy	Indianapolis
info	Information
init	Initialize
int	Intellect
intel	Intelligence
inv	Invite
ion	I don't
iv	Interview
jb/c	Just because
jbc	Just because
jc	Jewelcrafter
jelly	Jealous
kara	Karazhan
kb	Kilobyte
kgo	Ok, go
kl	Kool
kptondl	Keep it on the down low
ks	Kingslayer
kthx	OK, Thanks
kthxbai	OK, thanks, bye
kthxbi	OK thanks, bye
kw	Know
l8r	Later
laf	Laugh
laffing	Laughing
larper	Live action role-player
larping	Live action role-playing
legit	Legitimate
lib	liberal
lil	Little
ling	Zergling
lk	Like
lolz	Laugh out louds
ltr	Later
ltr	Letter
lu4ever	Love you forever
luzr	Loser
lv	Love
lv1	Leave one
lvl	Level
lyn	Lying
m	Male
m8	Mate
mab	Maybe
mag	Magazine
mats	Materials
mb	Megabyte
mbps	Megabits per second
mcds	McDonald's
md@u	Mad at you
md@ufn	Mad at you for now
me2	Me too
mech	Mechanical units
meds	Medications
meh	Whatever
mgmt	Management
mid	Middle
mid	Middle lane
min	Minute
mins	Minutes
mka	Make additions
mng	Manage
mngr	Manager
mo	More
mob	Mobile
mod	Moderator
mod	Modification
mofo	Mother F*****
morf	Male or female
mp5	Mana per five seconds
msft	Microsoft
msg	Message
mtg	Meeting
mth	Month
mut	Mutilate
muta	Mutalisks
mysp	MySpace
n	And
n	In
n/m	Nevermind
n8kd	Naked
nax	Naxxaramas
naxx	Naxxaramas
ne	Any
ne1	Anyone
ne1er	Anyone here
nemore	Anymore
nethng	Anything
neva	Never
neway	Anyway
newaze	Anyways
newb	Newbie
nite	Night
nm	Nevermind
nmtn	Nevermind that now
no	Know
no1	No one
nottie	Unattractive person
nuke	Nuclear missile
nvm	Nevermind
nvr	Never
nvrm	Nevermind
nxt	Next
obs	Obviously
obvi	Obviously
od	Overdose
ofc	Of course
oh	Overheard
onl	Online
op	Operator
op	Overpowered
orly	Oh really
ovie	Overlord
p/d	Per day
p/m	Per month
p/w	Password
p/w	Per week
p/y	Per year
pat	Patrol
perv	Pervert
pf	Preflop
pfr	Preflop raise
phobar	Photoshopped beyond all recognition
pic	Picture
pick	Pickaxe
pix	Pictures
pjs	Pajamas
pkmn	Pokémon
pl	Pinkliner
pld	Played
pls	Please
plz	Please
plzrd	Please read
pmsl	Piss myself laughing
posbl	Possible
pot	Potion
ppd	Postpartum depression
ppl	People
ppp	Peace
probs	Probably
proc	Programmed random occurrence
prof	Professor
promo	Promotion
prot	Protection
prvt	Private
ps	Postscript
puks	Pick up the kids
puter	Computer
pvt	Private
pw	Password
pwn2own	Pwn to Own Contest
qt	Cutie
qtpi	Cutie Pie
r	Are
r8	Rate
rad	Radical
rax	Barracks
rdnk	Redneck
rdy	Ready
re	Replay
ref	Referee
rents	Parents
rep	Reputation
resq	Rescue
resto	Restoration
rez	Resurrection
rgd	Regard
rgds	Regards
ridic	Ridiculous
rly	Really
rn	Run
rnt	Aren't
robo	Robotics Facility
rox	Rocks
rthx	Retweet thanks
rucmng	Are you coming?
s	Smiling
s	Yes
s/w	Software
samzd	Still amazed
sbux	Starbucks
sc2	StarCraft 2
sc2nite	StarCraft 2 tonight?
sec	Second
sesh	Session
sfx	Special effects
sh^	Shut up
shat	Shattrath City
shatt	Shattrath City
shmup	Shoot 'em up
sho	Sure
shud	Should
sic	Spelling incorrect
sitch	Situation
sitrep	Situation report
siul8r	See you later
sk8	Skate
sk8r	Skater
sok	It's OK
sorta	Sort of
sp	Spelling
spec	Specialization
spk	Speak
spkr	Speaker
srry	Sorry
srs	Serious
srsly	Seriously
ss	Miss
ss	Screenshot
stache	Mustache
stats	Statistics
stpd	Stupid
str	Strength
str8	Straight
subs	Subwoofers
sum1	Someone
sup	What's up
sux	Sucks
sw	Software
sync	Synchronize
sysadmin	System administrator
sysop	System operator (chat monitor)
t2go	Time to go
t2ul8er	Talk to you later
t2ul8r	Talk to you later
tab	Talking about
tard	Retard
tat	Tattoo
tb	Terabyte
td	Touchdown
tech	Technology
tel	Telephone
teme	Tell me
terribad	Terrible bad
thnq	Thank you
tho	Though
thru	Through
tht	That
thx	Thanks
tl	Tell
tlk	Talk
tmrw	Tomorrow
tnx	Thanks
toss	Protoss
tots	Totally
tourney	Tournament
tru	True
tru dat	True that
ttly	Totally
tude	Attitude
twds	Towards
tx	Thanks
txt	Text
txtin	Texting
u	You
u-ok	You OK?
u/l	Upload
ul	Unlucky
ul	Upload
ult	Ultimate ability
uno	You know
uok	You OK?
ura	You're a
uraqt	You are a cutie
ure	You're
urs4e	Yours forever
urw	You're welcome
us	Ultrasound
vent	Ventrilo
vfx	Visual effects
vid	Video
vlog	Video blog
vlogger	Video blogger
vry	Very
vs	Versus
w/	With
w/e	Whatever
w/o	Without
w84m	Wait for me
w8am	Wait a minute
w8ing	Waiting
w8n	Waiting
wa	What
waggro	Wife aggro
wags	Wife and girlfriends
wan2	Want to
wat	What
watev	Whatever
watevs	Whatever
we	Whatever
wei	Whatever idiot
weva	Whatever
wg	Wintergrasp
whatevs	Whatever
wht	What
wk	Week
wkd	Wicked
wknd	Weekend
wld	Would
wlk	Walk
woot	Wonderful loot
wot	What
wrisit	Where is it
wrudoin	What are you doing?
wsp	Whisper
wtv	Whatever
wup	What's up?
wut	What
xb	Xbox
xbf	Ex-boyfriend
xbl	Xbox Live
xc	Cross country
xep	Except
xfer	Transfer
xgf	Ex-girlfriend
xit	Exit
xitb4ili	Exit before I lose it
xlnt	Excellent
xmute	Transmute
xp	Experience
xt	Cross training
yall	You all
ydg	Ya dig
yrp	Europe
yrs	Years
